package com.basicks.smoketherapp.settings;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.View;
import com.basicks.smoketherapp.R;

public class AboutDialog extends DialogFragment {

	public AboutDialog() {
		// Empty constructor for fragment
	}

	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		View view = getActivity().getLayoutInflater().inflate(R.layout.about_dialog, null);
		builder.setView(view);
		builder.setTitle("About");
		builder.setNegativeButton("Close", new OnClickListener() {
			
			@Override
			public void onClick(DialogInterface dialog, int which) {
				getDialog().dismiss();
			}
		});
		return builder.create();
	}
}