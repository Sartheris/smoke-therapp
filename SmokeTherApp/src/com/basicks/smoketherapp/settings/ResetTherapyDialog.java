package com.basicks.smoketherapp.settings;

import java.io.File;
import com.basicks.smoketherapp.R;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.CountDownTimer;
import android.util.Log;
import android.widget.Toast;

public class ResetTherapyDialog {

	private Context context;

	public ResetTherapyDialog(Context context) {
		this.context = context;
	}

	public void showDialog() {
		AlertDialog.Builder dialog = new AlertDialog.Builder(context);
		dialog.setTitle("Reset Therapy");
		dialog.setMessage(context.getString(R.string.reset_therapy));
		dialog.setCancelable(true);
		dialog.setPositiveButton(context.getString(R.string.button_reset_yes), new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				clearApplicationData();
				CountDownTimer timer = new CountDownTimer(3000, 1000) {

					@Override
					public void onTick(long millisUntilFinished) {
						Toast.makeText(context, "Therapy reset successfully! Please restart the app for the changes to take effect", Toast.LENGTH_LONG).show();
					}

					@Override
					public void onFinish() {
						android.os.Process.killProcess(android.os.Process.myPid());
					}
				};
				timer.start();
			}
		});
		dialog.setNegativeButton(context.getString(R.string.button_reset_no), new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dlg, int which) {
				dlg.cancel();
			}
		});
		AlertDialog al = dialog.create();
		al.show();
	}

	private void clearApplicationData() {
		File cache = context.getCacheDir();
		File appDir = new File(cache.getParent());
		if (appDir.exists()) {
			String[] children = appDir.list();
			for (String s : children) {
				if (!s.equals("lib")) {
					deleteDir(new File(appDir, s));
					Log.i("TAG", "**************** File /data/data/APP_PACKAGE/" + s + " DELETED *******************");
				}
			}
		}
	}

	private static boolean deleteDir(File dir) {
		if (dir != null && dir.isDirectory()) {
			String[] children = dir.list();
			for (int i = 0; i < children.length; i++) {
				boolean success = deleteDir(new File(dir, children[i]));
				if (!success) {
					return false;
				}
			}
		}
		return dir.delete();
	}
}