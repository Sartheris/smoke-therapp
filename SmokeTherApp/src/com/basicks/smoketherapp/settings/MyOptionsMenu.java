package com.basicks.smoketherapp.settings;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.nio.channels.FileChannel;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Environment;
import android.widget.Toast;

import com.basicks.smoketherapp.R;
import com.basicks.smoketherapp.models.Constants;

public class MyOptionsMenu {

	public static void getContactMenu(Context context) {
		Intent i = new Intent(Intent.ACTION_SEND);
		i.setType("message/rfc822");
		i.putExtra(Intent.EXTRA_EMAIL, new String[] { Constants.BASICKS_EMAIL });
		i.putExtra(Intent.EXTRA_SUBJECT, context.getString(R.string.app_name));
		try {
			context.startActivity(Intent.createChooser(i, "Send e-mail..."));
		} catch (android.content.ActivityNotFoundException ex) {
			Toast.makeText(context, "There are no email clients installed.", Toast.LENGTH_LONG).show();
		}
	}

	public static void sendDatabase(Context context) {
		try {
			File data = Environment.getDataDirectory();
			File sd = Environment.getExternalStorageDirectory();
			if (sd.canWrite()) {
				String currentDBPath = "//data//" + context.getPackageName() + "//databases//SmokeTherApp";
				String backupDBPath = "SmokeTherApp.db";
				File currentDB = new File(data, currentDBPath);
				File backupDB = new File(sd, backupDBPath);
				if (currentDB.exists()) {
					FileChannel src = new FileInputStream(currentDB).getChannel();
					FileChannel dst = new FileOutputStream(backupDB).getChannel();
					dst.transferFrom(src, 0, src.size());
					src.close();
					dst.close();
				}
				Intent i = new Intent(Intent.ACTION_SEND);
				i.putExtra(Intent.EXTRA_EMAIL, new String[] { "sartheris@gmail.com" });
				i.putExtra(Intent.EXTRA_SUBJECT, "SMOKETHERAPP DB");
				i.putExtra(Intent.EXTRA_TEXT, getDevice());
				i.setType("application/octet-stream");
				i.putExtra(Intent.EXTRA_STREAM, Uri.fromFile(backupDB));
				context.startActivity(Intent.createChooser(i, "Send e-mail"));
			}
		} catch (Exception e) {
		}
	}

	private static String getDevice() {
		String deviceName = android.os.Build.MODEL;
		String deviceMan = android.os.Build.MANUFACTURER;
		int osVersion = android.os.Build.VERSION.SDK_INT;
		String phone = deviceMan + " - " + deviceName + " - " + osVersion;
		return phone;
	}
}