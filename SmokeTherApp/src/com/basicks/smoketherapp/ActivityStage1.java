package com.basicks.smoketherapp;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Vibrator;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.LocalBroadcastManager;
import android.text.Html;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.basicks.smoketherapp.models.Commons;
import com.basicks.smoketherapp.models.Constants;
import com.basicks.smoketherapp.models.MyCalendar;
import com.basicks.smoketherapp.models.MyPrefs;
import com.basicks.smoketherapp.service.CountdownService;
import com.basicks.smoketherapp.settings.MyOptionsMenu;
import com.basicks.smoketherapp.settings.ResetTherapyDialog;
import com.basicks.smoketherapp.sqlite.DataHelper;
import com.basicks.smoketherapp.statistics.ShareContent;
import com.basicks.smoketherapp.statistics.StatisticsStage1;
import com.todddavies.components.progressbar.ProgressWheel;

public class ActivityStage1 extends FragmentActivity {

	private static CountDownTimer timer;
	private FrameLayout fl;
	private Button buttonStatistics;
	private TextView tvProgress, tvStage;
	private TextView tvRegisterCigarette, tvWarning, tvDebugCounter;
	private DataHelper db;
	private ProgressWheel pw;
	private long timeUntilStageTwo, nextWakeUp;
	private BroadcastReceiver receiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {
			if (intent.hasExtra("stopActivity")) {
				finish();
				startActivity(new Intent(ActivityStage1.this, ActivityStage2.class));
			} else {
				String time = intent.getStringExtra("formattedTime");
				updateTimer(time);
			}
		}
	};

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_stage1);
		Commons.showOptionsMenu(this);
		db = new DataHelper(this);
		initViews();
		if (MyPrefs.isStageTwoTimerStarted()) {
			tvStage.setText(getString(R.string.stage) + " " + 2);
			enableProgressWheel(false);
			showStatisticsButton();
			tvWarning.setVisibility(View.GONE);
			tvRegisterCigarette.setCompoundDrawables(null, null, null, null);
		} else {
			if (checkForThreeDays(db.getFirstCigarette())) {
				pw.setOnTouchListener(null);
				prepareButtonsForStageTwo();
			} else {
				tvRegisterCigarette.setText(Html.fromHtml(getString(R.string.button_reg_cigarette_1) + " "
						+ "<font color=\"#c6cf24\"><big>" + "2 " + "</big></font>" + " " + getString(R.string.button_reg_cigarette_2)));
				pw.setOnTouchListener(new OnTouchListener() {

					@Override
					public boolean onTouch(View v, MotionEvent event) {
						switch (event.getAction()) {
						case MotionEvent.ACTION_DOWN:
							startRegisterCigaretteTimer();
							break;
						case MotionEvent.ACTION_UP:
							cancelTimer();
							enableProgressWheel(true);
							break;
						case MotionEvent.ACTION_CANCEL:
							cancelTimer();
							enableProgressWheel(true);
							break;
						default:
							break;
						}
						return true;
					}
				});
			}
		}
	}

	private void showStatisticsButton() {
		tvWarning.setVisibility(View.GONE);
		buttonStatistics.setVisibility(View.VISIBLE);
		buttonStatistics.setOnTouchListener(Commons.getOnTouchListener(this, buttonStatistics));
		buttonStatistics.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent intent = new Intent(ActivityStage1.this, StatisticsStage1.class);
				intent.putExtra("stageOne", true);
				startActivity(intent);
			}
		});
	}

	private void prepareButtonsForStageTwo() {
		tvStage.setText(getString(R.string.stage) + " " + 2);
		tvRegisterCigarette.setText(getString(R.string.begin_stage_2) + " " + "\n\n(hold for 2sec)");
		tvRegisterCigarette.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
		pw.setOnTouchListener(new OnTouchListener() {

			@Override
			public boolean onTouch(View v, MotionEvent event) {
				switch (event.getAction()) {
				case MotionEvent.ACTION_DOWN:
					startStageTwoButton();
					break;
				case MotionEvent.ACTION_UP:
					cancelTimer();
					break;
				case MotionEvent.ACTION_CANCEL:
					cancelTimer();
					break;
				default:
					break;
				}
				return true;
			}
		});
	}

	@Override
	protected void onResume() {
		super.onResume();
		MyPrefs.setActivity1Visible(true, this);
		LocalBroadcastManager.getInstance(this).registerReceiver(receiver, new IntentFilter(Constants.CIG_COUNTDOWN_INTENT));
		tvProgress.setLayoutParams(Commons.getButtonWidth(this));
	}

	@Override
	protected void onStop() {
		super.onStop();
		MyPrefs.setActivity1Visible(false, this);
		LocalBroadcastManager.getInstance(this).unregisterReceiver(receiver);
	}

	private void enableProgressWheel(boolean b) {
		pw.resetCount();
		pw.setEnabled(b);
		pw.clearFocus();
	}

	private void startRegisterCigaretteTimer() {
		long time;
		if (Constants.DEBUG) {
			time = 20;
		} else {
			time = 2000;
		}
		timer = new CountDownTimer(time, 54) {
			public void onFinish() {
				successfullCigarette();
				cancelTimer();
				startCigaretteButtonEnableTimer();
				if (Constants.DEBUG) {
					tvDebugCounter.setText("Cigarettes: " + db.getCigarettesNumber());
				}
			}

			@Override
			public void onTick(long millisUntilFinished) {
				for (int i = 0; i < 10; i++) {
					pw.incrementProgress();
				}
			}
		}.start();
	}

	// Re-enable button after certain period
	private void startCigaretteButtonEnableTimer() {
		long time;
		if (Constants.DEBUG) {
			time = 20;
		} else {
			time = 20000;
		}
		timer = new CountDownTimer(time, 1000) {
			public void onFinish() {
				enableProgressWheel(true);
				Drawable top = getResources().getDrawable(R.drawable.cigarette_icon);
				tvRegisterCigarette.setCompoundDrawablesWithIntrinsicBounds(null, top, null, null);
				tvRegisterCigarette.setText(Html.fromHtml(getString(R.string.button_reg_cigarette_1) + " "
						+ "<font color=\"#c6cf24\"><big>" + "2 " + "</big></font>" + " " + getString(R.string.button_reg_cigarette_2)));
			}

			@Override
			public void onTick(long millisUntilFinished) {
			}
		}.start();
	}

	private void cancelTimer() {
		if (timer != null) {
			timer.cancel();
		}
		pw.resetCount();
	}

	private void startStageTwoButton() {
		timer = new CountDownTimer(2000, 54) {
			public void onFinish() {
				cancelTimer();
				tvWarning.setVisibility(View.GONE);
				showStatisticsButton();
				nextWakeUp = MyCalendar.getTimeUntilNextDay() + System.currentTimeMillis();
				MyPrefs.setWakeUp(nextWakeUp, ActivityStage1.this);
				MyPrefs.setActivity1Visible(true, ActivityStage1.this);
				timeUntilStageTwo = nextWakeUp - System.currentTimeMillis();
				Commons.createTherapyProgram(ActivityStage1.this);
				enableProgressWheel(false);
				Intent intent = new Intent(ActivityStage1.this, CountdownService.class);
				intent.putExtra("timeUntilStageTwo", timeUntilStageTwo);
				startService(intent);
			}

			@Override
			public void onTick(long millisUntilFinished) {
				for (int i = 0; i < 10; i++) {
					pw.incrementProgress();
				}
			}
		}.start();
	}

	private void updateTimer(String time) {
		tvRegisterCigarette.setText(Html.fromHtml(getString(R.string.countdown_to_stage_2) + "<br><br><font color=\"#dde913\"><big>" + time
				+ "</big></font>"));
	}

	private void successfullCigarette() {
		enableProgressWheel(false);
		fl.startAnimation(Commons.getFadeAnimation(20000, 0.5f));
		Vibrator vib = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
		vib.vibrate(100);
		Drawable top = getResources().getDrawable(R.drawable.success);
		tvRegisterCigarette.setCompoundDrawablesWithIntrinsicBounds(null, top, null, null);
		tvRegisterCigarette.setText(getString(R.string.successfull_cigarette));
		db.registerCigarette(System.currentTimeMillis());
		if (checkForThreeDays(db.getFirstCigarette())) {
			pw.setOnTouchListener(null);
			prepareButtonsForStageTwo();
		}
	}

	private boolean checkForThreeDays(long firstCigarette) {
		if (Constants.DEBUG && MyPrefs.isDebugComplete()) {
			return true;
		}
		if (System.currentTimeMillis() - firstCigarette > Constants.THREE_DAYS && firstCigarette != 0) {
			return true;
		} else {
			return false;
		}
	}

	/** Options Menu */
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		if (Constants.DEBUG) {
			getMenuInflater().inflate(R.menu.my_menu_debug_1, menu);
		} else {
			getMenuInflater().inflate(R.menu.my_menu, menu);
		}
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.action_tutorial:
			startActivity(new Intent(this, ActivityTutorial.class));
			return true;
		case R.id.action_about:
			startActivity(new Intent(this, ActivityAbout.class));
			return true;
		case R.id.action_contact:
			MyOptionsMenu.getContactMenu(this);
			return true;
		case R.id.action_share:
			ShareContent.postDownload(this);
			return true;
		case R.id.action_donate:
			startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(Constants.DONATE_URL)));
			return true;
		case R.id.action_reset:
			ResetTherapyDialog dlg = new ResetTherapyDialog(this);
			dlg.showDialog();
			return true;
		case R.id.action_exit:
			finish();
			return true;
		case R.id.complete:
			MyPrefs.setDebugComplete(true, this);
			finish();
			startActivity(new Intent(getApplicationContext(), ActivityStartup.class));
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}

	private void initViews() {
		tvRegisterCigarette = (TextView) findViewById(R.id.tvRegisterCigarette);
		buttonStatistics = (Button) findViewById(R.id.buttonStatistics);
		tvProgress = (TextView) findViewById(R.id.tvProgress);
		tvStage = (TextView) findViewById(R.id.tvStage);
		tvWarning = (TextView) findViewById(R.id.tvWarning);
		pw = (ProgressWheel) findViewById(R.id.pw_spinner);
		if (Commons.getDisplayMetrics(this) < 200) {
			pw.setRimWidth(5);
			pw.setBarWidth(4);
		} else if (Commons.getDisplayMetrics(this) >= 320) {
			pw.setRimWidth(12);
			pw.setBarWidth(9);
		}
		fl = (FrameLayout) findViewById(R.id.fl);
		if (Constants.DEBUG) {
			tvDebugCounter = (TextView) findViewById(R.id.tvDebugCounter);
			tvDebugCounter.setVisibility(View.VISIBLE);
			tvDebugCounter.setText("Cigarettes: " + db.getCigarettesNumber());
		}
	}
}