package com.basicks.smoketherapp.sqlite;

import android.provider.BaseColumns;

public class DataSchema {

	public static final String CIGARETTE_SMOKED_STAGE_ONE = "cigarette_smoked";
	public static final String THERAPY_STAGE_TWO = "therapy_stage_two";

	public static final class CigaretteSmokedColumns implements BaseColumns {

		private CigaretteSmokedColumns() {
			// Empty constructor
		}

		public static final String SMOKED = "smoked";

	}
	
	public static final class TherapyColumns implements BaseColumns {

		private TherapyColumns() {
			// Empty constructor
		}

		public static final String DAYS_TAKEN = "days_taken";
		public static final String STAGE = "stage";
		public static final String CIGARETTE_SMOKED = "cig_smoked";
		public static final String CIGARETTE_ALLOWED = "cig_allowed";
		public static final String CIGARETTE_FOR_THIS_DAY = "cig_for_this_day";

	}
}