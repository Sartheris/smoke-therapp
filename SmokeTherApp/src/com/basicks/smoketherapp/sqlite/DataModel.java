package com.basicks.smoketherapp.sqlite;

import com.basicks.smoketherapp.sqlite.DataSchema.CigaretteSmokedColumns;
import com.basicks.smoketherapp.sqlite.DataSchema.TherapyColumns;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class DataModel extends SQLiteOpenHelper {

	private static final int DATABASE_VERSION = 1;
	private static final String DATABASE_NAME = "SmokeTherApp";

	private static final String CIGARETTE_SMOKED_CREATE_TABLE = "CREATE TABLE " + DataSchema.CIGARETTE_SMOKED_STAGE_ONE + " ("
			+ CigaretteSmokedColumns._ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " + CigaretteSmokedColumns.SMOKED + " VARCHAR);";

	private static final String THERAPY_CREATE_TABLE = "CREATE TABLE " + DataSchema.THERAPY_STAGE_TWO + " (" + TherapyColumns._ID
			+ " INTEGER PRIMARY KEY AUTOINCREMENT, " + TherapyColumns.DAYS_TAKEN + " VARCHAR, " + TherapyColumns.STAGE + " INTEGER, "
			+ TherapyColumns.CIGARETTE_SMOKED + " VARCHAR, " + TherapyColumns.CIGARETTE_ALLOWED + " INTEGER, "
			+ TherapyColumns.CIGARETTE_FOR_THIS_DAY + " INTEGER);";

	public DataModel(Context c) {
		super(c, DATABASE_NAME, null, DATABASE_VERSION);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		db.execSQL("PRAGMA foreign_keys = ON;");
		db.execSQL(CIGARETTE_SMOKED_CREATE_TABLE);
		db.execSQL(THERAPY_CREATE_TABLE);
		Log.e("Database created", "Database created");
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		Log.e("Database model", "Upgrading database from version " + oldVersion + " to " + newVersion + ", which will destroy all old data");
		db.execSQL("DROP TABLE IF EXISTS " + DataSchema.CIGARETTE_SMOKED_STAGE_ONE);
		db.execSQL("DROP TABLE IF EXISTS " + DataSchema.THERAPY_STAGE_TWO);
		onCreate(db);
	}
}