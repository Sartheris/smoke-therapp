package com.basicks.smoketherapp.sqlite;

import java.util.ArrayList;
import com.basicks.smoketherapp.models.Commons;
import com.basicks.smoketherapp.models.MyPrefs;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

public class DataHelper {

	private Context mContext;
	private DataModel dbModel;

	public DataHelper(Context context) {
		this.mContext = context;
		this.dbModel = new DataModel(this.mContext);
	}

	public void registerCigarette(long cigaretteSmoked) {
		ContentValues values = new ContentValues();
		values.put(DataSchema.CigaretteSmokedColumns.SMOKED, cigaretteSmoked);
		SQLiteDatabase db = dbModel.getReadableDatabase();
		db.insert(DataSchema.CIGARETTE_SMOKED_STAGE_ONE, null, values);
	}

	public long getFirstCigarette() {
		SQLiteDatabase db = dbModel.getWritableDatabase();
		String selectQuery = "SELECT * FROM " + DataSchema.CIGARETTE_SMOKED_STAGE_ONE + " WHERE " + DataSchema.CigaretteSmokedColumns._ID
				+ " = 1";
		Cursor c = db.rawQuery(selectQuery, null);
		long l;
		if (c.moveToFirst()) {
			l = c.getLong(c.getColumnIndex(DataSchema.CigaretteSmokedColumns.SMOKED));
		} else {
			l = 0;
		}
		c.close();
		return l;
	}

	public int getCigarettesNumber() {
		SQLiteDatabase db = dbModel.getReadableDatabase();
		Cursor c = db.rawQuery("SELECT * FROM " + DataSchema.CIGARETTE_SMOKED_STAGE_ONE, null);
		int i = c.getCount();
		return i;
	}

	public void insertTherapyProgramArray(ArrayList<int[]> cigarettesAllowed) {
		SQLiteDatabase db = dbModel.getReadableDatabase();
		Cursor c = db.rawQuery("SELECT * FROM " + DataSchema.THERAPY_STAGE_TWO, null);
		if (c.getCount() == 0) {
			ContentValues values = new ContentValues();
			for (int i = 0; i < cigarettesAllowed.size(); i++) {
				int[] j = cigarettesAllowed.get(i);
				for (int k = 0; k < j.length; k++) {
					values.put(DataSchema.TherapyColumns.STAGE, (i + 2));
					values.put(DataSchema.TherapyColumns.CIGARETTE_ALLOWED, j[k]);
					values.put(DataSchema.TherapyColumns.CIGARETTE_FOR_THIS_DAY, j[k]);
					db.insert(DataSchema.THERAPY_STAGE_TWO, null, values);
				}
			}
		}
	}

	public int getCigarettesForToday(int dayOfTherapy) {
		SQLiteDatabase db = dbModel.getReadableDatabase();
		int cig;
		Cursor c = db.rawQuery("SELECT * FROM " + DataSchema.THERAPY_STAGE_TWO + " WHERE " + DataSchema.TherapyColumns._ID + " = "
				+ dayOfTherapy, null);
		if (c.moveToFirst()) {
			cig = c.getInt(c.getColumnIndex(DataSchema.TherapyColumns.CIGARETTE_ALLOWED));
			c.close();
			return cig;
		} else {
			// return c is empty
			c.close();
			return -1;
		}
	}

	public void updateCigaretteForToday(int cigarettesForToday, int dayOfTherapy) {
		SQLiteDatabase db = dbModel.getWritableDatabase();
		String query = "UPDATE " + DataSchema.THERAPY_STAGE_TWO + " SET " + DataSchema.TherapyColumns.CIGARETTE_ALLOWED + " = "
				+ cigarettesForToday + " WHERE _id = " + dayOfTherapy;
		db.execSQL(query);
	}

	public void addCigarettesToSameDay(int dayOfTherapy) {
		SQLiteDatabase db = dbModel.getWritableDatabase();
		int i = getCigarettesForThisDay(dayOfTherapy);
		String query = "UPDATE " + DataSchema.THERAPY_STAGE_TWO + " SET " + DataSchema.TherapyColumns.CIGARETTE_ALLOWED + " = " + i
				+ " WHERE _id = " + dayOfTherapy;
		db.execSQL(query);
	}

	private int getCigarettesForThisDay(int dayOfTherapy) {
		SQLiteDatabase db = dbModel.getReadableDatabase();
		Cursor c = db.rawQuery("SELECT cig_for_this_day FROM " + DataSchema.THERAPY_STAGE_TWO + " WHERE " + DataSchema.TherapyColumns._ID
				+ " = " + dayOfTherapy, null);
		if (c.moveToFirst()) {
			int cig = c.getInt(c.getColumnIndex(DataSchema.TherapyColumns.CIGARETTE_FOR_THIS_DAY));
			c.close();
			return cig;
		} else {
			c.close();
			return 0;
		}
	}

	public int getTherapyStage(int dayOfTherapy) {
		// SELECT stage FROM therapy_stage_two WHERE _id = 1
		int stage = 0;
		SQLiteDatabase db = dbModel.getReadableDatabase();
		Cursor c = db.rawQuery("SELECT " + DataSchema.TherapyColumns.STAGE + " FROM " + DataSchema.THERAPY_STAGE_TWO + " WHERE "
				+ DataSchema.TherapyColumns._ID + " = " + dayOfTherapy, null);
		if (c.moveToFirst()) {
			stage = c.getInt(c.getColumnIndex(DataSchema.TherapyColumns.STAGE));
		}
		c.close();
		return stage;
	}

	// vry6ta dnite do sega prez koito e pu6eno
	public int getTherapyUntilNow(int dayOfTherapy) {
		// SELECT * FROM therapy_stage_two WHERE cig_allowed = 0
		SQLiteDatabase db = dbModel.getReadableDatabase();
		Cursor c = db.rawQuery("SELECT * FROM therapy_stage_two WHERE cig_allowed = 0", null);
		int daysCompleted = c.getCount();
		c.close();
		return daysCompleted;
	}

	public void updateTherapyDaysTaken(Context context, int i) {
		int dayOfTherapy = MyPrefs.getDayOfTherapy();
		int daysTaken = getDaysTakenToCompleteCurrentStep(dayOfTherapy) + i;
		if (daysTaken == 0) {
			daysTaken = 1;
		}
		SQLiteDatabase db = dbModel.getWritableDatabase();
		String query = "UPDATE " + DataSchema.THERAPY_STAGE_TWO + " SET " + DataSchema.TherapyColumns.DAYS_TAKEN + " = " + daysTaken
				+ " WHERE _id = " + dayOfTherapy;
		db.execSQL(query);
	}

	private int getDaysTakenToCompleteCurrentStep(int dayOfTherapy) {
		SQLiteDatabase db = dbModel.getReadableDatabase();
		int cig;
		// SELECT days_taken FROM therapy_stage_two WHERE _id = 1
		Cursor c = db.rawQuery("SELECT days_taken FROM " + DataSchema.THERAPY_STAGE_TWO + " WHERE " + DataSchema.TherapyColumns._ID + " = "
				+ dayOfTherapy, null);
		if (c.moveToFirst()) {
			cig = c.getInt(c.getColumnIndex(DataSchema.TherapyColumns.DAYS_TAKEN));
			c.close();
			return cig;
		} else {
			c.close();
			return 0;
		}
	}

	public void updateCigarettesSmokedToday(int cigarettesSmokedToday, int dayOfTherapy) {
		SQLiteDatabase db = dbModel.getWritableDatabase();
		String query = "UPDATE " + DataSchema.THERAPY_STAGE_TWO + " SET " + DataSchema.TherapyColumns.CIGARETTE_SMOKED + " = "
				+ cigarettesSmokedToday + " WHERE _id = " + dayOfTherapy;
		db.execSQL(query);
	}

	public int getTherapyLength() {
		SQLiteDatabase db = dbModel.getReadableDatabase();
		Cursor c = db.rawQuery("SELECT * FROM therapy_stage_two WHERE _id", null);
		int i = c.getCount();
		c.close();
		return i;
	}

	public int getDaysLeftToComplete() {
		SQLiteDatabase db = dbModel.getReadableDatabase();
		Cursor c = db.rawQuery("SELECT _id FROM therapy_stage_two WHERE days_taken is null", null);
		int i = c.getCount();
		c.close();
		return i;
	}

	public int getFirstDayCigarettes() {
		SQLiteDatabase db = dbModel.getReadableDatabase();
		Cursor c = db.rawQuery("SELECT cig_for_this_day FROM therapy_stage_two WHERE _id = 1", null);
		int cig = 0;
		if (c.moveToFirst()) {
			cig = c.getInt(c.getColumnIndex(DataSchema.TherapyColumns.CIGARETTE_FOR_THIS_DAY));
		}
		c.close();
		return cig;
	}

	public int getDaysTakenToCompleteStage(int stage) {
		// SELECT days_taken FROM therapy_stage_two WHERE stage = 2
		SQLiteDatabase db = dbModel.getReadableDatabase();
		Cursor c = db.rawQuery("SELECT days_taken FROM therapy_stage_two WHERE stage = " + stage, null);
		int daysTakenToComplete = 0;
		if (c.moveToFirst()) {
			do {
				int j = c.getInt(0);
				daysTakenToComplete = daysTakenToComplete + j;
			} while (c.moveToNext());
		}
		c.close();
		return daysTakenToComplete;
	}

	public String getNicotineLoweredForStageTwo() {
		int cigarettes = getFirstDayCigarettes();
		int cigarettesToRemoveForStage = Commons.getCigarettesToRemoveStageTwoAndThree(cigarettes, false);
		double loweredNicotine = cigarettesToRemoveForStage * (100 / (double) cigarettes);
		String s = String.format("%.2f", loweredNicotine);
		return s;
	}

	public String getNicotineLoweredForStageThree() {
		int cigarettes = getFirstDayCigarettes();
		int cigarettesToRemoveForStage = Commons.getCigarettesToRemoveStageFour(cigarettes);
		double loweredNicotine = (cigarettes - cigarettesToRemoveForStage) * (100 / (double) cigarettes);
		String s = String.format("%.2f", loweredNicotine);
		return s;
	}

	public String getNicotineLoweredForStageFour() {
		double cigarettes = getFirstDayCigarettes();
		double loweredNicotine = (cigarettes - 1) * (100 / cigarettes);
		String s = String.format("%.2f", loweredNicotine);
		return s;
	}

	public int getConstantCigsForToday(int dayOfTherapy) {
		// SELECT cig_for_this_day FROM therapy_stage_two WHERE _id = 3
		SQLiteDatabase db = dbModel.getReadableDatabase();
		int constantCigsForTOday = 0;
		Cursor c = db.rawQuery("SELECT cig_for_this_day FROM therapy_stage_two WHERE _id = " + dayOfTherapy, null);
		if (c.moveToFirst()) {
			constantCigsForTOday = c.getInt(c.getColumnIndex(DataSchema.TherapyColumns.CIGARETTE_FOR_THIS_DAY));
			c.close();
		}
		return constantCigsForTOday;
	}
}