package com.basicks.smoketherapp.service;

import com.basicks.smoketherapp.models.MyPrefs;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.widget.Toast;

public class AutoStart extends BroadcastReceiver {

	public void onReceive(Context context, Intent arg1) {
		MyPrefs.loadSharedPreferences(context);
		if (MyPrefs.isStageOneCompleted() && !MyPrefs.isTherapyFinished()) {
			Intent intent = new Intent(context, CigaretteService.class);
			context.startService(intent);
			Toast.makeText(context, "Smoke TherApp Started", Toast.LENGTH_LONG).show();
			Log.v("CigService", "autostart");
		}
	}
}