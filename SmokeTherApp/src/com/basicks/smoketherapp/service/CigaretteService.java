package com.basicks.smoketherapp.service;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.support.v4.content.LocalBroadcastManager;
import com.basicks.smoketherapp.models.Commons;
import com.basicks.smoketherapp.models.Constants;
import com.basicks.smoketherapp.models.MyPrefs;
import com.basicks.smoketherapp.notification.CigaretteNotification;
import com.basicks.smoketherapp.notification.StageNotification;
import com.basicks.smoketherapp.sqlite.DataHelper;

public class CigaretteService extends Service {

	private DataHelper db;
	private int cigarettesSmokedToday, cigarettesBeingAllowedForToday, allowedCigarette;
	private int cigarettesForToday, dayOfTherapy, therapyStage;
	private long oneCigaretteTime, nextCigaretteTime, wakeUp;
	private boolean threadRunning;
	private LocalBroadcastManager broadcaster;
	private Thread myThread;

	@Override
	public void onCreate() {
		super.onCreate();
		db = new DataHelper(this);
		broadcaster = LocalBroadcastManager.getInstance(this);
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		threadRunning = false;
		saveVariables();
	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		if (intent != null) {
			initVariables();
			startForeground(Constants.FOREGROUND_ID, ForegroundService.buildForeground(this, allowedCigarette));
			threadRunning = true;
			if (myThread == null) {
				myThread = new Thread() {
					@Override
					public void run() {
						while (threadRunning) {
							cigaretteCheck();
						}
					}
				};
				myThread.start();
			}
		}
		return START_REDELIVER_INTENT;
	}

	private void cigaretteCheck() {
		try {
			Thread.sleep(10000); // TODO varni go na 5000
			if (MyPrefs.isGivingCigarettes() && nextCigaretteTime <= System.currentTimeMillis()) {
				initVariables();
				if (cigarettesBeingAllowedForToday >= (cigarettesSmokedToday + cigarettesForToday)) {
					MyPrefs.setGivingCigarettes(false, this);
					saveVariables();
				} else {
					nextCigaretteTime = wakeUp + (oneCigaretteTime * (cigarettesBeingAllowedForToday + 1));
					MyPrefs.setNextCigaretteTime(nextCigaretteTime, this);
					initVariables();
					if (!MyPrefs.isActivity2Visible()) {
						CigaretteNotification.sendNotification(CigaretteService.this);
					}
					cigarettesBeingAllowedForToday++;
					allowedCigarette++;
					saveVariables();
					startForeground(Constants.FOREGROUND_ID, ForegroundService.buildForeground(this, allowedCigarette));
					broadcaster.sendBroadcast(new Intent(Constants.CIG_INTENT));
				}
			}
			if (System.currentTimeMillis() >= (wakeUp + Constants.ONE_DAY)) {
				initVariables();
				if (MyPrefs.isExtraCigaretteSmoked()) {
					// reinit the same day
					db.addCigarettesToSameDay(dayOfTherapy);
					db.updateTherapyDaysTaken(this, 1);
				} else {
					// next day
					db.updateTherapyDaysTaken(this, 0);
					db.updateCigarettesSmokedToday(cigarettesSmokedToday, dayOfTherapy);
					dayOfTherapy++;
				}
				resetVariables();
				initVariables();
				startForeground(Constants.FOREGROUND_ID, ForegroundService.buildForeground(this, allowedCigarette));
				broadcaster.sendBroadcast(new Intent(Constants.CIG_INTENT));
			}
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	private void initVariables() {
		dayOfTherapy = MyPrefs.getDayOfTherapy();
		therapyStage = MyPrefs.getTherapyStage();
		int i = Commons.getTherapyStage(this);
		if (therapyStage < i) {
			MyPrefs.setTherapyStage(i, this);
			StageNotification.sendNotification(this);
		}
		cigarettesForToday = db.getCigarettesForToday(dayOfTherapy);
		if (dayOfTherapy > db.getTherapyLength()) {
			finishTherapy();
		} else {
			cigarettesSmokedToday = MyPrefs.getCigarettesSmokedToday();
			cigarettesBeingAllowedForToday = MyPrefs.getCigarettesBeingAllowedForToday();
			allowedCigarette = MyPrefs.getAllowedCigarette();
			int wakeHours = MyPrefs.getWakeHours();
			int constantCigsForToday = db.getConstantCigsForToday(dayOfTherapy);
			wakeUp = MyPrefs.getWakeUp();
			oneCigaretteTime = ((long) (((float) wakeHours / constantCigsForToday) * Constants.ONE_HOUR)); // TODO
			nextCigaretteTime = MyPrefs.getNextCigaretteTime();
			if (nextCigaretteTime == 0) {
				nextCigaretteTime = wakeUp + oneCigaretteTime;
				MyPrefs.setNextCigaretteTime(nextCigaretteTime, this);
			}
		}
	}

	private void saveVariables() {
		db.updateCigaretteForToday(cigarettesForToday, dayOfTherapy);
		db.updateCigarettesSmokedToday(cigarettesSmokedToday, dayOfTherapy);
		MyPrefs.setCigarettesBeingAllowedForToday(cigarettesBeingAllowedForToday, this);
		MyPrefs.setCigarettesSmokedToday(cigarettesSmokedToday, this);
		MyPrefs.setAllowedCigarette(allowedCigarette, this);
		MyPrefs.setDayOfTherapy(dayOfTherapy, this);
	}

	private void resetVariables() {
		MyPrefs.setCigarettesSmokedToday(0, this);
		MyPrefs.setCigarettesBeingAllowedForToday(1, this);
		MyPrefs.setAllowedCigarette(1, this);
		MyPrefs.setExtraCigaretteDrawn(false, this);
		MyPrefs.setExtraCigaretteSmoked(false, this);
		MyPrefs.setDayOfTherapy(dayOfTherapy, this);
		MyPrefs.setHasActivityBeenStartedToday(false, this);
		MyPrefs.setWakeUp((wakeUp + Constants.ONE_DAY), this);
		wakeUp = wakeUp + Constants.ONE_DAY;
		MyPrefs.setGivingCigarettes(true, this);
		MyPrefs.setNextCigaretteTime(0, this);
	}

	private void finishTherapy() {
		saveVariables();
		threadRunning = false;
		MyPrefs.setIsTherapyFinished(true, this);
		stopSelf();
		broadcaster.sendBroadcast(new Intent(Constants.CIG_INTENT));
	}

	@Override
	public IBinder onBind(Intent intent) {
		return null;
	}
}