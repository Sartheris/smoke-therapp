package com.basicks.smoketherapp.service;

import android.app.Notification;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.NotificationCompat;
import com.basicks.smoketherapp.ActivityStage1;
import com.basicks.smoketherapp.ActivityStage2;
import com.basicks.smoketherapp.R;

public class ForegroundService {
	
	public static Notification buildForeground(Context context, int allowedCigarette) {
		NotificationCompat.Builder b = new NotificationCompat.Builder(context);
		b.setOngoing(true)
		 .setContentIntent(getPendingIntent(context, ActivityStage2.class))
		 .setContentTitle(context.getString(R.string.app_name))
		 .setContentText(context.getString(R.string.cig_allowance) + " " + allowedCigarette)
		 .setSmallIcon(R.drawable.ic_launcher);
		return b.build();
	}
	
	public static Notification buildCountdown(Context context, String time) {
		NotificationCompat.Builder b = new NotificationCompat.Builder(context);
		b.setOngoing(true)
		 .setContentIntent(getPendingIntent(context, ActivityStage1.class))
		 .setContentTitle(context.getString(R.string.app_name))
		 .setContentText(context.getString(R.string.countdown_to_stage_2) + " - " + time)
		 .setSmallIcon(R.drawable.ic_launcher);
		return b.build();
	}

	private static PendingIntent getPendingIntent(Context context, Class<?> class1) {
		Intent intent = new Intent(context, class1);
		PendingIntent pIntent = PendingIntent.getActivity(context, 0, intent, 0);
		return pIntent;
	}
}