package com.basicks.smoketherapp.service;

import java.util.concurrent.TimeUnit;
import com.basicks.smoketherapp.models.Constants;
import com.basicks.smoketherapp.models.MyPrefs;
import com.basicks.smoketherapp.notification.StageNotification;
import android.app.Service;
import android.content.Intent;
import android.os.CountDownTimer;
import android.os.IBinder;
import android.support.v4.content.LocalBroadcastManager;

public class CountdownService extends Service {

	private static CountDownTimer timerStageTwo;
	private LocalBroadcastManager broadcaster;

	@Override
	public void onCreate() {
		super.onCreate();
		broadcaster = LocalBroadcastManager.getInstance(this);
	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		if (intent != null) {
			MyPrefs.setStageTwoTimerStarted(true, this);
			long timeUntilStageTwo = intent.getLongExtra("timeUntilStageTwo", 0);
			startStageTwoTimer(timeUntilStageTwo);
		}
		return START_REDELIVER_INTENT;
	}

	private void startStageTwoTimer(long timeUntilStageTwo) {
		timerStageTwo = new CountDownTimer(timeUntilStageTwo, 100) {
			public void onFinish() {
				MyPrefs.setStageOneCompleted(true, CountdownService.this);
				StageNotification.sendNotification(CountdownService.this);
				if (timerStageTwo != null) {
					timerStageTwo.cancel();
				}
				if (MyPrefs.isActivity1Visible()) {
					broadcaster.sendBroadcast(new Intent(Constants.CIG_COUNTDOWN_INTENT).putExtra("stopActivity", true));
				} else {
					startService(new Intent(CountdownService.this, CigaretteService.class));
				}
				stopSelf();
			}

			@Override
			public void onTick(long millisUntilFinished) {
				long s = TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished);
				int hours = (int) (s / 60);
				int minutes = (int) (s % 60);
				int seconds = (int) ((millisUntilFinished / 1000) % 60);
				String formattedTime = String.format("%d:%02d:%02d", hours, minutes, seconds);
				broadcaster.sendBroadcast(new Intent(Constants.CIG_COUNTDOWN_INTENT).putExtra("formattedTime", formattedTime));
				startForeground(Constants.FOREGROUND_ID, ForegroundService.buildCountdown(CountdownService.this, formattedTime));
			}
		}.start();
	}

	@Override
	public IBinder onBind(Intent intent) {
		return null;
	}
}