package com.basicks.smoketherapp.models;

import java.util.Locale;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.content.res.Resources;

public class MyPrefs {

	// � ���� ���� �� �������� ������ SharedPreferences ��� ���������� �� ������������, � �� ������ � ������� � ����������,
	// ������ ����� ��� �� �� ���� �� �����
	private static SharedPreferences settings;
	private static SharedPreferences.Editor editor;
	private static long wakeUp, nextCigaretteTime;
	private static int dayOfTherapy, therapyStage, wakeHours, progressPercents;
	private static int allowedCigarette, cigarettesSmokedToday, cigarettesBeingAllowedForToday;
	private static String wakeUpTime, language;
	private static boolean isActivity1Visible, hasActivityBeenStartedToday, isTherapyFinalTimer, isActivity2Visible;
	private static boolean stageOneCompleted, initialSetupDone, isTherapyFinished, personalInfoDone, debugComplete;
	private static boolean stageTwoTimerStarted, alarmsSet, givingCigarettes;
	private static boolean extraCigaretteDrawn, extraCigaretteSmoked;

	/** Initial Loading */
	public static void loadSharedPreferences(Context context) {
		dayOfTherapy = Integer.parseInt(getPref(context, "dayOfTherapy", "1"));
		wakeUpTime = getPref(context, "wakeUpTime", "8:00");
		language = getPref(context, "language", "en");
		cigarettesSmokedToday = Integer.parseInt(getPref(context, "cigarettesSmokedToday", 0 + ""));
		therapyStage = Integer.parseInt(getPref(context, "therapyStage", 2 + ""));
		wakeHours = Integer.parseInt(getPref(context, "wakeHours", "8"));
		allowedCigarette = Integer.parseInt(getPref(context, "allowedCigarette", 1 + ""));
		progressPercents = Integer.parseInt(getPref(context, "progressPercents", 0 + ""));
		cigarettesBeingAllowedForToday = Integer.parseInt(getPref(context, "cigarettesBeingAllowedForToday", 1 + ""));
		extraCigaretteDrawn = getPrefBoolean(context, "extraCigaretteDrawn", false);
		extraCigaretteSmoked = getPrefBoolean(context, "extraCigaretteSmoked", false);
		givingCigarettes = getPrefBoolean(context, "givingCigarettes", true);
		isActivity1Visible = getPrefBoolean(context, "isActivity1Visible", false);
		isActivity2Visible = getPrefBoolean(context, "isActivity2Visible", false);
		hasActivityBeenStartedToday = getPrefBoolean(context, "hasActivityBeenStartedToday", false);
		isTherapyFinalTimer = getPrefBoolean(context, "isTherapyFinalTimer", false);
		stageOneCompleted = getPrefBoolean(context, "stageOneCompleted", false);
		alarmsSet = getPrefBoolean(context, "alarmsSet", false);
		initialSetupDone = getPrefBoolean(context, "initialSetupDone", false);
		isTherapyFinished = getPrefBoolean(context, "therapyIsFinished", false);
		personalInfoDone = getPrefBoolean(context, "personalInfoDone", false);
		debugComplete = getPrefBoolean(context, "debugComplete", false);
		stageTwoTimerStarted = getPrefBoolean(context, "stageTwoTimerStarted", false);
		wakeUp = Long.parseLong(getPref(context, "wakeUp", 0 + ""));
		nextCigaretteTime = Long.parseLong(getPref(context, "nextCigaretteTime", 0 + ""));
	}

	public static void setApplicationLocale(String language, Context context) {
		Locale locale = new Locale(language, language);
		Locale.setDefault(locale);
		Configuration config = new Configuration();
		config.locale = locale;
		Resources resources = context.getResources();
		resources.updateConfiguration(config, resources.getDisplayMetrics());
	}

	public static boolean isGivingCigarettes() {
		return givingCigarettes;
	}

	public static void setGivingCigarettes(boolean givingCigarettes, Context context) {
		MyPrefs.givingCigarettes = givingCigarettes;
		setPrefBoolean(context, "givingCigarettes", givingCigarettes);
	}

	public static boolean isStageTwoTimerStarted() {
		return stageTwoTimerStarted;
	}

	public static void setStageTwoTimerStarted(boolean stageTwoTimerStarted, Context context) {
		MyPrefs.stageTwoTimerStarted = stageTwoTimerStarted;
		setPrefBoolean(context, "stageTwoTimerStarted", stageTwoTimerStarted);
	}

	public static boolean isDebugComplete() {
		return debugComplete;
	}

	public static void setDebugComplete(boolean debugComplete, Context context) {
		MyPrefs.debugComplete = debugComplete;
		setPrefBoolean(context, "debugComplete", debugComplete);
	}

	public static boolean alarmsSet() {
		return alarmsSet;
	}

	public static void setAlarmsSet(boolean alarmsSet, Context context) {
		MyPrefs.alarmsSet = alarmsSet;
		setPrefBoolean(context, "alarmsSet", alarmsSet);
	}

	public static boolean isPersonalInfoDone() {
		return personalInfoDone;
	}

	public static void setPersonalInfoDone(boolean personalInfoDone, Context context) {
		MyPrefs.personalInfoDone = personalInfoDone;
		setPrefBoolean(context, "personalInfoDone", personalInfoDone);
	}

	public static boolean isTherapyFinished() {
		return isTherapyFinished;
	}

	public static void setIsTherapyFinished(boolean isTherapyFinished, Context context) {
		MyPrefs.isTherapyFinished = isTherapyFinished;
		setPrefBoolean(context, "isTherapyFinished", isTherapyFinished);
	}

	public static boolean isInitialSetupDone() {
		return initialSetupDone;
	}

	public static void setInitialSetupDone(boolean initialSetupDone, Context context) {
		MyPrefs.initialSetupDone = initialSetupDone;
		setPrefBoolean(context, "initialSetupDone", initialSetupDone);
	}

	public static boolean isStageOneCompleted() {
		return stageOneCompleted;
	}

	public static void setStageOneCompleted(boolean stageOneCompleted, Context context) {
		MyPrefs.stageOneCompleted = stageOneCompleted;
		setPrefBoolean(context, "stageOneCompleted", stageOneCompleted);
	}

	public static boolean isTherapyFinalTimer() {
		return isTherapyFinalTimer;
	}

	public static void setTherapyFinalTimer(boolean isTherapyFinalTimer, Context context) {
		MyPrefs.isTherapyFinalTimer = isTherapyFinalTimer;
		setPrefBoolean(context, "isTherapyFinalTimer", isTherapyFinalTimer);
	}

	public static boolean hasActivityBeenStartedToday() {
		return hasActivityBeenStartedToday;
	}

	public static void setHasActivityBeenStartedToday(boolean hasActivityBeenStartedToday, Context context) {
		MyPrefs.hasActivityBeenStartedToday = hasActivityBeenStartedToday;
		setPrefBoolean(context, "hasActivityBeenStartedToday", hasActivityBeenStartedToday);
	}

	public static boolean isActivity1Visible() {
		return isActivity1Visible;
	}

	public static void setActivity1Visible(boolean isActivity1Visible, Context context) {
		MyPrefs.isActivity1Visible = isActivity1Visible;
		setPrefBoolean(context, "isActivity1Visible", isActivity1Visible);
	}

	public static boolean isActivity2Visible() {
		return isActivity2Visible;
	}

	public static void setActivity2Visible(boolean isActivity2Visible, Context context) {
		MyPrefs.isActivity2Visible = isActivity2Visible;
		setPrefBoolean(context, "isActivity2Visible", isActivity2Visible);
	}

	public static boolean isExtraCigaretteDrawn() {
		return extraCigaretteDrawn;
	}

	public static void setExtraCigaretteDrawn(boolean extraCigaretteDrawn, Context context) {
		MyPrefs.extraCigaretteDrawn = extraCigaretteDrawn;
		setPrefBoolean(context, "extraCigaretteDrawn", extraCigaretteDrawn);
	}

	public static boolean isExtraCigaretteSmoked() {
		return extraCigaretteSmoked;
	}

	public static void setExtraCigaretteSmoked(boolean extraCigaretteSmoked, Context context) {
		MyPrefs.extraCigaretteSmoked = extraCigaretteSmoked;
		setPrefBoolean(context, "extraCigaretteSmoked", extraCigaretteSmoked);
	}

	public static int getCigarettesBeingAllowedForToday() {
		return cigarettesBeingAllowedForToday;
	}

	public static void setCigarettesBeingAllowedForToday(int cigarettesBeingAllowedForToday, Context context) {
		MyPrefs.cigarettesBeingAllowedForToday = cigarettesBeingAllowedForToday;
		setPref(context, "cigarettesBeingAllowedForToday", cigarettesBeingAllowedForToday + "");
	}

	public static int getProgressPercents() {
		return progressPercents;
	}

	public static void setProgressPercents(int progressPercents, Context context) {
		MyPrefs.progressPercents = progressPercents;
		setPref(context, "progressPercents", progressPercents + "");
	}

	public static int getAllowedCigarette() {
		return allowedCigarette;
	}

	public static void setAllowedCigarette(int allowedCigarette, Context context) {
		MyPrefs.allowedCigarette = allowedCigarette;
		setPref(context, "allowedCigarette", allowedCigarette + "");
	}

	public static int getTherapyStage() {
		return therapyStage;
	}

	public static void setTherapyStage(int therapyStage, Context context) {
		MyPrefs.therapyStage = therapyStage;
		setPref(context, "therapyStage", therapyStage + "");
	}

	public static int getCigarettesSmokedToday() {
		return cigarettesSmokedToday;
	}

	public static void setCigarettesSmokedToday(int cigarettesSmokedToday, Context context) {
		MyPrefs.cigarettesSmokedToday = cigarettesSmokedToday;
		setPref(context, "cigarettesSmokedToday", cigarettesSmokedToday + "");
	}

	public static String getLanguage() {
		return language;
	}

	public static void setLanguage(String language, Context context) {
		MyPrefs.language = language;
		setPref(context, "language", language);
	}

	public static String getWakeUpTime() {
		return wakeUpTime;
	}

	public static void setWakeUpTime(String wakeUpTime, Context context) {
		MyPrefs.wakeUpTime = wakeUpTime;
		setPref(context, "wakeUpTime", wakeUpTime);
	}

	public static int getDayOfTherapy() {
		return dayOfTherapy;
	}

	public static void setDayOfTherapy(int dayOfTherapy, Context context) {
		MyPrefs.dayOfTherapy = dayOfTherapy;
		setPref(context, "dayOfTherapy", dayOfTherapy + "");
	}

	public static long getWakeUp() {
		return wakeUp;
	}

	public static void setWakeUp(long wakeUp, Context context) {
		MyPrefs.wakeUp = wakeUp;
		setPref(context, "wakeUp", wakeUp + "");
	}

	public static long getNextCigaretteTime() {
		return nextCigaretteTime;
	}

	public static void setNextCigaretteTime(long nextCigaretteTime, Context context) {
		MyPrefs.nextCigaretteTime = nextCigaretteTime;
		setPref(context, "nextCigaretteTime", nextCigaretteTime + "");
	}

	public static int getWakeHours() {
		return wakeHours;
	}

	public static void setWakeHours(int wakeHours, Context context) {
		MyPrefs.wakeHours = wakeHours;
		setPref(context, "wakeHours", wakeHours + "");
	}

	/** Get String from SharedPreferences */
	private static String getPref(Context context, String value, String defaultValue) {
		settings = context.getSharedPreferences(Constants.PREFS, 0);
		return settings.getString(value, defaultValue);
	}

	private static void setPref(Context context, String preference, String value) {
		settings = context.getSharedPreferences(Constants.PREFS, 0);
		editor = settings.edit();
		editor.putString(preference, value);
		editor.commit();
	}

	// Get boolean from SharedPreferences
	private static boolean getPrefBoolean(Context context, String value, boolean defaultValue) {
		settings = context.getSharedPreferences(Constants.PREFS, 0);
		return settings.getBoolean(value, defaultValue);
	}

	private static void setPrefBoolean(Context context, String preference, boolean value) {
		settings = context.getSharedPreferences(Constants.PREFS, 0);
		editor = settings.edit();
		editor.putBoolean(preference, value);
		editor.commit();
	}
}