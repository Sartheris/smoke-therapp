package com.basicks.smoketherapp.models;

public class Constants {

	public static final boolean DEBUG = false;

	public static final String APP_NAME = "Smoke TherApp";
	public static final String CIG_INTENT = "cigarette";
	public static final String CIG_COUNTDOWN_INTENT = "cigarette_countdown";
	public static final String PREFS = "smoke_therapp";
	public static final long ONE_HOUR = 3600000;
	public static final long ONE_DAY = 86400000;
	public static final long THREE_DAYS = 259200000;
	public static final int FOREGROUND_ID = 156;
	public static final String BASICKS_EMAIL = "info@studiobasicks.com";
	public static final String DONATE_URL = "https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=8DH4JBSNP93H6";
	public static final String SHARE_TEXT = "Struggle to quit smoking? No Problem! Download Smoke TherApp for free on Google Play - ";
	public static final String DOWNLOAD_LINK = "https://play.google.com/store/apps/details?id=com.basicks.smoketherapp";
}