package com.basicks.smoketherapp.models;

import java.util.Calendar;

public class MyCalendar {

	private static Calendar calendar;

	public static long getTimeUntilNextDay() {
		calendar = getCalendar();
		long l = (calendar.getTimeInMillis() - System.currentTimeMillis());
		if (l > Constants.ONE_DAY) {
			return (l - Constants.ONE_DAY);
		} else {
			return (calendar.getTimeInMillis() - System.currentTimeMillis());
		}
	}

	public static long getNextWakeUp() {
		calendar = getCalendar();
		return calendar.getTimeInMillis();
	}

	private static Calendar getCalendar() {
		Calendar c = Calendar.getInstance();
		String s = MyPrefs.getWakeUpTime();
		String[] separated = s.split("\\:");
		String hours = separated[0];
		String minutes = separated[1];
		c.add(Calendar.DAY_OF_MONTH, 1);
		c.set(Calendar.HOUR_OF_DAY, Integer.parseInt(hours));
		c.set(Calendar.MINUTE, Integer.parseInt(minutes));
		c.set(Calendar.SECOND, 0);
		c.set(Calendar.MILLISECOND, 0);
		return c;
	}
}