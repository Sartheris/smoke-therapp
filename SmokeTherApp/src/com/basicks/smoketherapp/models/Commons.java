package com.basicks.smoketherapp.models;

import java.lang.reflect.Field;
import java.util.ArrayList;
import android.content.Context;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.View.OnTouchListener;
import android.view.ViewGroup.LayoutParams;
import android.view.WindowManager;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.LinearLayout;

import com.basicks.smoketherapp.R;
import com.basicks.smoketherapp.sqlite.DataHelper;

public class Commons {

	private static int cigarettePerDay;
	private static ArrayList<int[]> cigarettesAllowed;
	private static DataHelper db;

	public static void createTherapyProgram(Context context) {
		db = new DataHelper(context);
		float d = (float) db.getCigarettesNumber() / 3;
		cigarettePerDay = (int) Math.round(d);
		if (cigarettePerDay > 39) {
			cigarettePerDay = 39;
		} else if (cigarettePerDay < 4) {
			cigarettePerDay = 4;
		}
		cigarettesAllowed = new ArrayList<int[]>();
		int cigaretteToRemoveStageTwo = getCigarettesToRemoveStageTwoAndThree(cigarettePerDay, false);
		int cigaretteToRemoveStageThree = getCigarettesToRemoveStageTwoAndThree(cigarettePerDay, true);
		int cigaretteToRemoveStageFour = getCigarettesToRemoveStageFour(cigarettePerDay);
		int stageTwoDays = cigaretteToRemoveStageTwo * 2;
		getStageTwo(stageTwoDays);
		int stageThreeDays = cigaretteToRemoveStageThree * 2;
		getStageThree(stageThreeDays);
		int stageFourDays = (cigaretteToRemoveStageFour - 1) * 3;
		getStageFour(stageFourDays);
		db.insertTherapyProgramArray(cigarettesAllowed);
	}

	// Stage Two and Three are similar
	private static void getStageTwo(int stageTwoDays) {
		int[] i = new int[stageTwoDays];
		int counter = 0;
		for (int j = cigarettePerDay; counter < i.length; j--) {
			i[counter] = j;
			counter++;
			if (counter < i.length) {
				i[counter] = j;
				counter++;
			}
		}
		cigarettePerDay = i[stageTwoDays - 1] - 1;
		cigarettesAllowed.add(i);
	}

	private static void getStageThree(int stageThreeDays) {
		int[] i = new int[stageThreeDays];
		int counter = 0;
		for (int j = cigarettePerDay; counter < i.length; j--) {
			i[counter] = j;
			counter++;
			if (counter < i.length) {
				i[counter] = j;
				counter++;
			}
		}
		cigarettePerDay = i[stageThreeDays - 1] - 1;
		cigarettesAllowed.add(i);
	}

	private static void getStageFour(int stageFourDays) {
		int[] i = new int[stageFourDays];
		int counter = 0;
		for (int j = cigarettePerDay; counter < i.length; j--) {
			i[counter] = j;
			counter++;
			if (counter < i.length) {
				i[counter] = j;
				counter++;
			}
			if (counter < i.length) {
				i[counter] = j;
				counter++;
			}
		}
		cigarettesAllowed.add(i);
	}

	// Returns the number of cigarettes needed to be removed on STAGE 2 and
	// STAGE 3
	// true if stage3 is required, because numbers are almost the same
	public static int getCigarettesToRemoveStageTwoAndThree(int cigaretteNumber, boolean stageThreeRequested) {
		switch (cigaretteNumber) {
		case 4:
			return 1;
		case 5:
			if (stageThreeRequested) {
				return 2;
			} else {
				return 1;
			}
		case 6:
			return 2;
		case 7:
			return 2;
		case 8:
			return 3;
		case 9:
			return 3;
		case 10:
			return 3;
		case 11:
			return 4;
		case 12:
			return 4;
		case 13:
			return 4;
		case 14:
			return 5;
		case 15:
			return 5;
		case 16:
			return 5;
		case 17:
			return 6;
		case 18:
			return 6;
		case 19:
			return 6;
		case 20:
			return 7;
		case 21:
			return 7;
		case 22:
			return 7;
		case 23:
			return 8;
		case 24:
			return 8;
		case 25:
			return 8;
		case 26:
			return 9;
		case 27:
			return 9;
		case 28:
			return 9;
		case 29:
			return 10;
		case 30:
			return 10;
		case 31:
			return 10;
		case 32:
			return 11;
		case 33:
			return 11;
		case 34:
			return 11;
		case 35:
			return 12;
		case 36:
			return 12;
		case 37:
			return 12;
		case 38:
			return 13;
		case 39:
			return 13;
		default:
			return 0;
		}
	}

	// Returns the number of cigarettes needed to be removed on STAGE 4
	public static int getCigarettesToRemoveStageFour(int cigaretteNumber) {
		switch (cigaretteNumber) {
		case 4:
			return 2;
		case 5:
			return 2;
		case 6:
			return 2;
		case 7:
			return 3;
		case 8:
			return 2;
		case 9:
			return 3;
		case 10:
			return 4;
		case 11:
			return 3;
		case 12:
			return 4;
		case 13:
			return 5;
		case 14:
			return 4;
		case 15:
			return 5;
		case 16:
			return 6;
		case 17:
			return 5;
		case 18:
			return 6;
		case 19:
			return 7;
		case 20:
			return 6;
		case 21:
			return 7;
		case 22:
			return 8;
		case 23:
			return 7;
		case 24:
			return 8;
		case 25:
			return 9;
		case 26:
			return 8;
		case 27:
			return 9;
		case 28:
			return 10;
		case 29:
			return 9;
		case 30:
			return 10;
		case 31:
			return 11;
		case 32:
			return 10;
		case 33:
			return 11;
		case 34:
			return 12;
		case 35:
			return 11;
		case 36:
			return 12;
		case 37:
			return 13;
		case 38:
			return 12;
		case 39:
			return 13;
		default:
			return 0;
		}
	}

	public static Animation getFadeAnimation(long l, float f) {
		Animation animation = new AlphaAnimation(f, 1);
		animation.setDuration(l);
		animation.setInterpolator(new LinearInterpolator());
		animation.setRepeatCount(0);
		return animation;
	}

	@SuppressWarnings("deprecation")
	public static LayoutParams getProgressWidth(Context context) {
		WindowManager windowManager = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
		Display display = windowManager.getDefaultDisplay();
		int width = display.getWidth();
		int height = display.getHeight();
		double onePercentWidth = ((double) width / 100);
		DataHelper db = new DataHelper(context);
		int dayOfTherapy = MyPrefs.getDayOfTherapy() - 1;
		int daysWholeTherapy = db.getTherapyLength();
		double rawPercents = ((double) (dayOfTherapy) / daysWholeTherapy) * 100;
		int percents = (int) rawPercents;
		MyPrefs.setProgressPercents(percents, context);
		int progressWidth = (int) (percents * onePercentWidth);
		int progressHeight = height / 14;
		FrameLayout.LayoutParams ll = new FrameLayout.LayoutParams(progressWidth, progressHeight);
		ll.setMargins(5, 5, 0, 5);
		return ll;
	}

	@SuppressWarnings("deprecation")
	public static LayoutParams getButtonWidth(Context context) {
		WindowManager windowManager = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
		Display display = windowManager.getDefaultDisplay();
		int width = display.getWidth();
		int height = display.getHeight();
		int progressHeight = height / 14;
		return new FrameLayout.LayoutParams(width, progressHeight);
	}

	public static int getTherapyStage(Context context) {
		db = new DataHelper(context);
		int dayOfTherapy = MyPrefs.getDayOfTherapy();
		return db.getTherapyStage(dayOfTherapy);
	}

	public static OnTouchListener getOnTouchListener(final Context context, final Button button) {
		OnTouchListener t = new OnTouchListener() {

			@Override
			public boolean onTouch(View v, MotionEvent event) {
				switch (event.getAction()) {
				case MotionEvent.ACTION_DOWN:
					button.setTextColor(context.getResources().getColor(R.color.white));
					button.setBackgroundColor(context.getResources().getColor(R.color.dark_yellow));
					button.invalidate();
					break;
				case MotionEvent.ACTION_CANCEL:
					button.setTextColor(context.getResources().getColor(R.color.black));
					button.setBackgroundColor(context.getResources().getColor(R.color.yellow));
					button.invalidate();
					break;
				case MotionEvent.ACTION_UP:
					button.setTextColor(context.getResources().getColor(R.color.black));
					button.setBackgroundColor(context.getResources().getColor(R.color.yellow));
					button.invalidate();
					break;
				default:
					break;
				}
				return false;
			}
		};
		return t;
	}

	public static void showOptionsMenu(Context context) {
		try {
			ViewConfiguration config = ViewConfiguration.get(context);
			Field menuKeyField = ViewConfiguration.class.getDeclaredField("sHasPermanentMenuKey");
			if (menuKeyField != null) {
				menuKeyField.setAccessible(true);
				menuKeyField.setBoolean(config, false);
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	public static float getDisplayMetrics(Context context) {
		DisplayMetrics d = context.getResources().getDisplayMetrics();
		return d.xdpi;
	}

	public static OnTouchListener getOnTouchListenerLL(final Context context, final LinearLayout llShare) {
		OnTouchListener t = new OnTouchListener() {

			@Override
			public boolean onTouch(View v, MotionEvent event) {
				switch (event.getAction()) {
				case MotionEvent.ACTION_DOWN:
					llShare.setBackgroundColor(context.getResources().getColor(R.color.dark_yellow));
					llShare.invalidate();
					break;
				case MotionEvent.ACTION_CANCEL:
					llShare.setBackgroundColor(context.getResources().getColor(R.color.yellow));
					llShare.invalidate();
					break;
				case MotionEvent.ACTION_UP:
					llShare.setBackgroundColor(context.getResources().getColor(R.color.yellow));
					llShare.invalidate();
					break;
				default:
					break;
				}
				return false;
			}
		};
		return t;
	}
}