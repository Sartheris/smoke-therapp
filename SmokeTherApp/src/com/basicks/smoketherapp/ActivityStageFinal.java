package com.basicks.smoketherapp;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v4.app.FragmentActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.widget.Button;
import android.widget.LinearLayout;
import com.basicks.smoketherapp.models.Commons;
import com.basicks.smoketherapp.models.Constants;
import com.basicks.smoketherapp.models.MyPrefs;
import com.basicks.smoketherapp.settings.MyOptionsMenu;
import com.basicks.smoketherapp.settings.ResetTherapyDialog;
import com.basicks.smoketherapp.sqlite.DataHelper;
import com.basicks.smoketherapp.statistics.Statistics;
import com.todddavies.components.progressbar.ProgressWheel;

public class ActivityStageFinal extends FragmentActivity {

	private static CountDownTimer timer;
	private LinearLayout llShare;
	private Button buttonStatistics;
	private ProgressWheel pw;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		DataHelper db = new DataHelper(this);
		db.getDaysTakenToCompleteStage(2);
	}

	@Override
	protected void onStart() {
		super.onStart();
		if (MyPrefs.isTherapyFinalTimer()) {
			setContentView(R.layout.activity_stage_final_finished);
			buttonStatistics = (Button) findViewById(R.id.buttonStatistics);
			buttonStatistics.setOnTouchListener(Commons.getOnTouchListener(ActivityStageFinal.this, buttonStatistics));
			buttonStatistics.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					Intent intent = new Intent(ActivityStageFinal.this, Statistics.class);
					startActivity(intent);
				}
			});
			llShare = (LinearLayout) findViewById(R.id.llShare);
			llShare.setOnTouchListener(Commons.getOnTouchListenerLL(this, llShare));
			llShare.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
					sharingIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);
					sharingIntent.setType("text/plain");
					String shareBody = "Here is the share content body";
					sharingIntent.putExtra(Intent.EXTRA_SUBJECT, "Subject Here");
					sharingIntent.putExtra(Intent.EXTRA_TEXT, shareBody);
					startActivity(Intent.createChooser(sharingIntent, "Share via"));

				}
			});
		} else {
			setContentView(R.layout.activity_stage_final);
			pw = (ProgressWheel) findViewById(R.id.pw_spinner);
			if (Commons.getDisplayMetrics(this) < 200) {
				pw.setRimWidth(5);
				pw.setBarWidth(4);
			} else if (Commons.getDisplayMetrics(this) >= 320) {
				pw.setRimWidth(12);
				pw.setBarWidth(9);
			}
			pw.setOnTouchListener(new OnTouchListener() {

				@Override
				public boolean onTouch(View v, MotionEvent event) {
					switch (event.getAction()) {
					case MotionEvent.ACTION_DOWN:
						startTimer();
						break;
					case MotionEvent.ACTION_UP:
						cancelTimer();
						break;
					case MotionEvent.ACTION_CANCEL:
						cancelTimer();
						break;
					default:
						break;
					}
					return true;
				}
			});
		}
	}

	private void startTimer() {
		timer = new CountDownTimer(2000, 54) {
			public void onFinish() {
				pw.resetCount();
				pw.clearFocus();
				timer.cancel();
				MyPrefs.setTherapyFinalTimer(true, ActivityStageFinal.this);
				Intent intent = new Intent(ActivityStageFinal.this, ActivityCongratulations.class);
				startActivity(intent);
			}

			@Override
			public void onTick(long millisUntilFinished) {
				for (int i = 0; i < 10; i++) {
					pw.incrementProgress();
				}

			}
		}.start();
	}

	private void cancelTimer() {
		if (timer != null) {
			timer.cancel();
		}
		pw.resetCount();
	}
	
	/** Options Menu */
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.my_menu, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.action_tutorial:
			// Tutorial
			return true;
		case R.id.action_about:
			startActivity(new Intent(this, ActivityAbout.class));
			return true;
		case R.id.action_contact:
			MyOptionsMenu.getContactMenu(this);
			return true;
		case R.id.action_share:
			// Intent share
			return true;
		case R.id.action_donate:
			startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(Constants.DONATE_URL)));
			return true;
		case R.id.action_reset:
			// Intent reset
			ResetTherapyDialog dlg = new ResetTherapyDialog(this);
			dlg.showDialog();
			return true;
		case R.id.action_exit:
			finish();
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}
}