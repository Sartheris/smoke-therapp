package com.basicks.smoketherapp.notification;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.support.v4.app.NotificationCompat;
import com.basicks.smoketherapp.ActivityStage2;
import com.basicks.smoketherapp.R;
import com.basicks.smoketherapp.models.MyPrefs;

public class CigaretteNotification {

	public static void sendNotification(Context context) {
		NotificationManager mNotifyManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
		NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(context);
		mBuilder.setContentTitle(context.getString(R.string.app_name))
				.setContentText("New cigarette allowed!")
				.setContentIntent(getPendingIntent(context, 0, ActivityStage2.class))
				.setSmallIcon(R.drawable.notif_icon)
				.setAutoCancel(true);
		mNotifyManager.notify(0, mBuilder.build());
		if (MyPrefs.hasActivityBeenStartedToday()) {
			MediaPlayer mediaPlayer = MediaPlayer.create(context, R.raw.notif);
			mediaPlayer.start();
			mediaPlayer.setOnCompletionListener(new OnCompletionListener() {

				@Override
				public void onCompletion(MediaPlayer mp) {
					mp.release();
					mp = null;
				}
			});
		}
	}

	private static PendingIntent getPendingIntent(Context context, int i, Class<?> class1) {
		Intent intent = new Intent(context, class1);
		PendingIntent pIntent = PendingIntent.getActivity(context, i, intent, 0);
		return pIntent;
	}
	
	public static void cancelNotification(Context context) {
		if (Context.NOTIFICATION_SERVICE != null) {
			NotificationManager nMgr = (NotificationManager) context.getApplicationContext().getSystemService(Context.NOTIFICATION_SERVICE);
			nMgr.cancel(0);
		}
	}
}