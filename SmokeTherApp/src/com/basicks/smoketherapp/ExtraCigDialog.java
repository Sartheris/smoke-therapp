package com.basicks.smoketherapp;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import com.basicks.smoketherapp.models.Commons;
import com.basicks.smoketherapp.models.MyPrefs;

public class ExtraCigDialog extends DialogFragment {

	private Button buttonContinue, buttonCancel;

	public ExtraCigDialog() {
		// Empty constructor for fragment
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fragment_warning, container);
		getDialog().setTitle(getString(R.string.warning));
		buttonContinue = (Button) rootView.findViewById(R.id.buttonContinue);
		buttonCancel = (Button) rootView.findViewById(R.id.buttonCancel);
		buttonContinue.setOnTouchListener(Commons.getOnTouchListener(getActivity(), buttonContinue));
		buttonContinue.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				MyPrefs.setExtraCigaretteDrawn(true, getActivity());
				dismiss();
				getActivity().finish();
				getActivity().startActivity(new Intent(getActivity(), ActivityStage2.class));
			}
		});
		buttonCancel.setOnTouchListener(Commons.getOnTouchListener(getActivity(), buttonCancel));
		buttonCancel.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				dismiss();
			}
		});
		return rootView;
	}
}