package com.basicks.smoketherapp.initialsetup;

import com.basicks.smoketherapp.R;
import com.basicks.smoketherapp.models.Commons;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;

public class Fragment2Welcome extends Fragment {
	
	private Button buttonProceed;

	public Fragment2Welcome() {
		// Empty constructor for fragment
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fragment_2_welcome, container, false);
		buttonProceed = (Button) rootView.findViewById(R.id.buttonProceed);
		buttonProceed.setOnTouchListener(Commons.getOnTouchListener(getActivity(), buttonProceed));
		buttonProceed.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.container, new Fragment3Tutorial()).commit();
			}
		});
		return rootView;
	}
}