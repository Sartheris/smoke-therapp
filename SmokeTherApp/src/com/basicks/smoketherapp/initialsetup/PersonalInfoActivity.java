package com.basicks.smoketherapp.initialsetup;

import com.basicks.smoketherapp.R;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.widget.TextView;

public class PersonalInfoActivity extends FragmentActivity {

	private TextView tvPersonalInfo;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_personal_info);
		tvPersonalInfo = (TextView) findViewById(R.id.tvPersonalInfo);
		tvPersonalInfo.setText(getString(R.string.personal_info) + " 1/3");
		if (savedInstanceState == null) {
			getSupportFragmentManager().beginTransaction().add(R.id.container, new PersonalInfo1()).commit();
		}
	}
}