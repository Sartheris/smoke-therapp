package com.basicks.smoketherapp.initialsetup;

import com.basicks.smoketherapp.R;
import com.basicks.smoketherapp.models.Commons;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.widget.Button;
import android.widget.TextView;

public class PersonalInfo1 extends Fragment {

	private Button buttonMale, buttonFemale, buttonContinue;

	public PersonalInfo1() {
		// Empty constructor for fragment
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fragment_personal_info_1, container, false);
		buttonMale = (Button) rootView.findViewById(R.id.buttonMale);
		buttonFemale = (Button) rootView.findViewById(R.id.buttonFemale);
		buttonContinue = (Button) rootView.findViewById(R.id.buttonContinue);
		buttonMale.setOnTouchListener(new OnTouchListener() {

			@Override
			public boolean onTouch(View v, MotionEvent event) {
				buttonMale.setPressed(true);
				buttonFemale.setPressed(false);
				buttonContinue.setBackgroundColor(getResources().getColor(R.color.yellow));
				buttonMale.setBackgroundResource(R.drawable.button_male_selected);
				buttonFemale.setBackgroundResource(R.drawable.button_female);
				buttonContinue.setEnabled(true);
				return true;
			}
		});
		buttonFemale.setOnTouchListener(new OnTouchListener() {

			@Override
			public boolean onTouch(View v, MotionEvent event) {
				buttonFemale.setPressed(true);
				buttonMale.setPressed(false);
				buttonContinue.setBackgroundColor(getResources().getColor(R.color.yellow));
				buttonFemale.setBackgroundResource(R.drawable.button_female_selected);
				buttonMale.setBackgroundResource(R.drawable.button_male);
				buttonContinue.setEnabled(true);
				return true;
			}
		});
		buttonContinue.setOnTouchListener(Commons.getOnTouchListener(getActivity(), buttonContinue));
		buttonContinue.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.container, new PersonalInfo2()).commit();
				TextView tvPersonalInfo = (TextView) getActivity().findViewById(R.id.tvPersonalInfo);
				tvPersonalInfo.setText(getString(R.string.personal_info) + " 2/3");
			}
		});
		return rootView;
	}
}