package com.basicks.smoketherapp.initialsetup;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RadioButton;

import com.basicks.smoketherapp.R;

public class Fragment3Tutorial extends Fragment {

	private Button buttonProceed;
	private RadioButton radio0, radio1, radio2, radio3;
	private MyFragmentAdapter adapter;
	private ViewPager mPager;

	public Fragment3Tutorial() {
		// Empty constructor for fragment
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fragment_3_tutorial,
				container, false);
		radio0 = (RadioButton) rootView.findViewById(R.id.radio0);
		radio1 = (RadioButton) rootView.findViewById(R.id.radio1);
		radio2 = (RadioButton) rootView.findViewById(R.id.radio2);
		radio3 = (RadioButton) rootView.findViewById(R.id.radio3);
		adapter = new MyFragmentAdapter(getFragmentManager());
		mPager = (ViewPager) rootView.findViewById(R.id.pager);
		mPager.setAdapter(adapter);
		mPager.setOnPageChangeListener(new OnPageChangeListener() {

			@Override
			public void onPageSelected(int arg0) {
				switch (arg0) {
				case 0:
					radio0.setChecked(true);
					radio1.setChecked(false);
					radio2.setChecked(false);
					radio3.setChecked(false);
					break;
				case 1:
					radio0.setChecked(false);
					radio1.setChecked(true);
					radio2.setChecked(false);
					radio3.setChecked(false);
					break;
				case 2:
					radio0.setChecked(false);
					radio1.setChecked(false);
					radio2.setChecked(true);
					radio3.setChecked(false);
					break;
				case 3:
					radio0.setChecked(false);
					radio1.setChecked(false);
					radio2.setChecked(false);
					radio3.setChecked(true);
					break;
				}

			}

			@Override
			public void onPageScrolled(int arg0, float arg1, int arg2) {
			}

			@Override
			public void onPageScrollStateChanged(int arg0) {
			}
		});
		/**
		 * buttonProceed = (Button) rootView.findViewById(R.id.buttonProceed);
		 * buttonProceed
		 * .setOnTouchListener(Commons.getOnTouchListener(getActivity(),
		 * buttonProceed)); buttonProceed.setOnClickListener(new
		 * OnClickListener() {
		 * 
		 * @Override public void onClick(View v) {
		 *           getActivity().getSupportFragmentManager
		 *           ().beginTransaction().replace(R.id.container, new
		 *           Fragment4Terms()).commit(); } });
		 */
		return rootView;
	}
}