package com.basicks.smoketherapp.initialsetup;

import com.basicks.smoketherapp.R;
import com.basicks.smoketherapp.models.Commons;
import com.basicks.smoketherapp.models.MyPrefs;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.Button;

public class Fragment4Terms extends Fragment {

	private Button agreeButton;

	public Fragment4Terms() {
		// Empty constructor for fragment
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fragment_4_terms, container, false);
		agreeButton = (Button) rootView.findViewById(R.id.agreeButton);
		agreeButton.setOnTouchListener(Commons.getOnTouchListener(getActivity(), agreeButton));
		agreeButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				MyPrefs.setInitialSetupDone(true, getActivity());
				getActivity().finish();
				startActivity(new Intent(getActivity(), PersonalInfoActivity.class));
			}
		});
		return rootView;
	}
}