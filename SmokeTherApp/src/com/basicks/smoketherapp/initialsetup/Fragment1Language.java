package com.basicks.smoketherapp.initialsetup;

import com.basicks.smoketherapp.R;
import com.basicks.smoketherapp.models.Commons;
import com.basicks.smoketherapp.models.MyPrefs;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.widget.Button;

public class Fragment1Language extends Fragment {

	private Button buttonEnglish, buttonSpanish, buttonContinue;
	private String language;

	public Fragment1Language() {
		// Empty constructor for fragment
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fragment_1_language, container, false);
		buttonEnglish = (Button) rootView.findViewById(R.id.buttonEnglish);
		buttonSpanish = (Button) rootView.findViewById(R.id.buttonSpanish);
		buttonContinue = (Button) rootView.findViewById(R.id.buttonContinue);
		buttonEnglish.setOnTouchListener(new OnTouchListener() {

			@Override
			public boolean onTouch(View v, MotionEvent event) {
				language = "en";
				MyPrefs.setApplicationLocale(language, getActivity());
				buttonEnglish.setPressed(true);
				buttonEnglish.setBackgroundColor(getResources().getColor(R.color.yellow));
				buttonEnglish.setTextColor(getResources().getColor(R.color.black));
				buttonSpanish.setPressed(false);
				buttonSpanish.setBackgroundColor(getResources().getColor(R.color.gray));
				buttonSpanish.setTextColor(getResources().getColor(R.color.white));
				buttonContinue.setEnabled(true);
				buttonContinue.setText(getString(R.string.button_continue));
				buttonContinue.setBackgroundColor(getResources().getColor(R.color.yellow));
				return true;
			}
		});
		buttonSpanish.setOnTouchListener(new OnTouchListener() {

			@Override
			public boolean onTouch(View v, MotionEvent event) {
				language = "es";
				MyPrefs.setApplicationLocale(language, getActivity());
				buttonSpanish.setPressed(true);
				buttonSpanish.setBackgroundColor(getResources().getColor(R.color.yellow));
				buttonSpanish.setTextColor(getResources().getColor(R.color.black));
				buttonEnglish.setPressed(false);
				buttonEnglish.setBackgroundColor(getResources().getColor(R.color.gray));
				buttonEnglish.setTextColor(getResources().getColor(R.color.white));
				buttonContinue.setEnabled(true);
				buttonContinue.setText(getString(R.string.button_continue));
				buttonContinue.setBackgroundColor(getResources().getColor(R.color.yellow));
				return true;
			}
		});
		buttonContinue.setOnTouchListener(Commons.getOnTouchListener(getActivity(), buttonContinue));
		buttonContinue.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				MyPrefs.setLanguage(language, getActivity());
				MyPrefs.setApplicationLocale(language, getActivity());
				getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.container, new Fragment2Welcome()).commit();
			}
		});
		return rootView;
	}
}