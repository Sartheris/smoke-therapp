package com.basicks.smoketherapp.initialsetup;

import android.media.MediaPlayer;
import android.media.MediaPlayer.OnPreparedListener;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.VideoView;

import com.basicks.smoketherapp.R;
import com.basicks.smoketherapp.models.Commons;
import com.basicks.smoketherapp.models.MyPrefs;

public class TutorialFragment3 extends Fragment {

	private TextView tvTutorial;
	private VideoView vvTutorial;
	private Button buttonContinue;

	public static Fragment newInstance(int position) {
		return new TutorialFragment3();
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View v = inflater.inflate(R.layout.tutorial_fragment_3, container, false);
		tvTutorial = (TextView) v.findViewById(R.id.tvTutorial);
		tvTutorial.setText(getString(R.string.tutorial_3));
		vvTutorial = (VideoView) v.findViewById(R.id.vvTutorial);
		Uri myUri = Uri.parse("android.resource://" + getActivity().getPackageName() + "/" + R.raw.video_tut_4);
		vvTutorial.setMediaController(null);
		vvTutorial.setVideoURI(myUri);
		vvTutorial.start();
		vvTutorial.setOnPreparedListener(new OnPreparedListener() {

			@Override
			public void onPrepared(MediaPlayer mp) {
				mp.setLooping(true);
			}
		});
		buttonContinue = (Button) v.findViewById(R.id.buttonContinue);
		if (!MyPrefs.isInitialSetupDone()) {
			buttonContinue.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.container, new Fragment4Terms()).commit();
				}
			});
		} else {
			buttonContinue.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					getActivity().finish();
				}
			});
		}
		buttonContinue.setOnTouchListener(Commons.getOnTouchListener(getActivity(), buttonContinue));
		return v;
	}
}