package com.basicks.smoketherapp.initialsetup;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import com.basicks.smoketherapp.R;
import com.basicks.smoketherapp.ActivityStage1;
import com.basicks.smoketherapp.models.Commons;
import com.basicks.smoketherapp.models.MyPrefs;

public class PersonalInfo4 extends Fragment {

	private Button buttonUnderstand;
	private TextView tvFinalInfo;

	public PersonalInfo4() {
		// Empty constructor for fragment
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fragment_personal_info_4, container, false);
		buttonUnderstand = (Button) rootView.findViewById(R.id.buttonUnderstand);
		tvFinalInfo = (TextView) rootView.findViewById(R.id.tvFinalInfo);
		tvFinalInfo.setText(Html.fromHtml(getString(R.string.final_info)));
		buttonUnderstand.setOnTouchListener(Commons.getOnTouchListener(getActivity(), buttonUnderstand));
		buttonUnderstand.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				MyPrefs.setPersonalInfoDone(true, getActivity());
				getActivity().finish();
				Intent i = new Intent(getActivity(), ActivityStage1.class);
				startActivity(i);
			}
		});
		return rootView;
	}
}