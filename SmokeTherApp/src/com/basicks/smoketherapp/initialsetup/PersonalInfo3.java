package com.basicks.smoketherapp.initialsetup;

import com.basicks.smoketherapp.R;
import com.basicks.smoketherapp.models.Commons;
import com.basicks.smoketherapp.models.MyPrefs;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.TimePicker;

public class PersonalInfo3 extends Fragment {

	private Button buttonContinue;
	private String wakeUpTime = "8:00";
	private TimePicker tp;

	public PersonalInfo3() {
		// Empty constructor for fragment
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fragment_personal_info_3, container, false);
		tp = (TimePicker) rootView.findViewById(R.id.timePicker);
		buttonContinue = (Button) rootView.findViewById(R.id.buttonContinue);
		buttonContinue.setOnTouchListener(Commons.getOnTouchListener(getActivity(), buttonContinue));
		buttonContinue.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				tp.clearFocus();
				int hour = tp.getCurrentHour();
				int min = tp.getCurrentMinute();
				wakeUpTime = hour + ":" + min;
				MyPrefs.setWakeUpTime(wakeUpTime, getActivity());
				getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.container, new PersonalInfo4()).commit();
				TextView textview = (TextView) getActivity().findViewById(R.id.tvPersonalInfo);
				textview.setText(getString(R.string.almost_there));
			}
		});
		return rootView;
	}
}