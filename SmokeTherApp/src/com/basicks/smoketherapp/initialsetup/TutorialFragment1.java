package com.basicks.smoketherapp.initialsetup;

import android.media.MediaPlayer;
import android.media.MediaPlayer.OnPreparedListener;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.VideoView;

import com.basicks.smoketherapp.R;

public class TutorialFragment1 extends Fragment {

	private TextView tvTutorial;
	private VideoView vvTutorial;

	public static Fragment newInstance(int position) {
		return new TutorialFragment1();
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View v = inflater.inflate(R.layout.tutorial_fragment_1, container, false);
		tvTutorial = (TextView) v.findViewById(R.id.tvTutorial);
		tvTutorial.setText(getString(R.string.tutorial_1));
		vvTutorial = (VideoView) v.findViewById(R.id.vvTutorial);
		Uri myUri = Uri.parse("android.resource://" + getActivity().getPackageName() + "/" + R.raw.video_tut_2);
		vvTutorial.setMediaController(null);
		vvTutorial.setVideoURI(myUri);
		vvTutorial.start();
		vvTutorial.setOnPreparedListener(new OnPreparedListener() {

			@Override
			public void onPrepared(MediaPlayer mp) {
				mp.setLooping(true);
			}
		});
		return v;
	}
}