package com.basicks.smoketherapp.initialsetup;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

public class MyFragmentAdapter extends FragmentPagerAdapter {
	public MyFragmentAdapter(FragmentManager fm) {
		super(fm);
	}

	@Override
	public int getCount() {
		return 4;
	}

	@Override
	public Fragment getItem(int position) {
		switch (position) {
		case 0:
			return TutorialFragment0.newInstance(position);
		case 1:
			return TutorialFragment1.newInstance(position);
		case 2:
			return TutorialFragment2.newInstance(position);
		case 3:
			return TutorialFragment3.newInstance(position);
		default:
			return TutorialFragment0.newInstance(position);
		}
	}
}