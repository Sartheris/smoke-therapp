package com.basicks.smoketherapp.initialsetup;

import com.basicks.smoketherapp.R;
import com.basicks.smoketherapp.models.Commons;
import com.basicks.smoketherapp.models.MyPrefs;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;

public class PersonalInfo2 extends Fragment {

	private Button buttonContinue;
	private Spinner spinnerSleepHours;
	private int sleepHours;

	public PersonalInfo2() {
		// Empty constructor for fragment
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fragment_personal_info_2, container, false);
		buttonContinue = (Button) rootView.findViewById(R.id.buttonContinue);
		spinnerSleepHours = (Spinner) rootView.findViewById(R.id.spinnerSleepHours);
		ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(getActivity(), R.array.sleep_hours, R.layout.spinner_item);
		adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		spinnerSleepHours.setAdapter(adapter);
		spinnerSleepHours.setSelection(4);
		spinnerSleepHours.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
				sleepHours = position + 4;
			}

			@Override
			public void onNothingSelected(AdapterView<?> parent) {
			}
		});
		buttonContinue.setOnTouchListener(Commons.getOnTouchListener(getActivity(), buttonContinue));
		buttonContinue.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				int wakeHours = 24 - sleepHours;
				MyPrefs.setWakeHours(wakeHours, getActivity());
				getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.container, new PersonalInfo3()).commit();
				TextView tvPersonalInfo = (TextView) getActivity().findViewById(R.id.tvPersonalInfo);
				tvPersonalInfo.setText(getString(R.string.personal_info) + " 3/3");
			}
		});
		return rootView;
	}
}