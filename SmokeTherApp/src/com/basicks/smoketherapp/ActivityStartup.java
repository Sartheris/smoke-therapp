package com.basicks.smoketherapp;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;

import com.basicks.smoketherapp.initialsetup.Fragment1Language;
import com.basicks.smoketherapp.initialsetup.PersonalInfoActivity;
import com.basicks.smoketherapp.models.MyPrefs;

public class ActivityStartup extends FragmentActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		try {
			myOnCreate(savedInstanceState);
		} catch (Exception e) {
			handleError();
		}
	}

	private void handleError() {
		try {
			Process process = Runtime.getRuntime().exec("logcat -d");
			BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(process.getInputStream()));
			StringBuilder log = new StringBuilder();
			String line;
			while ((line = bufferedReader.readLine()) != null) {
				log.append(line);
			}
			String fileName = "logcat_"+System.currentTimeMillis()+".txt";
		    File outputFile = new File(getExternalCacheDir(),fileName);
		    @SuppressWarnings("unused")
		    Process process2 = Runtime.getRuntime().exec("logcat -f "+outputFile.getAbsolutePath());
		} catch (IOException e) {
		}
	}

	private void myOnCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		// TODO tva da se premesti
		setContentView(R.layout.activity_startup);
		MyPrefs.loadSharedPreferences(ActivityStartup.this);
		MyPrefs.setApplicationLocale(MyPrefs.getLanguage(), ActivityStartup.this);
		if (MyPrefs.isInitialSetupDone()) {
			if (MyPrefs.isPersonalInfoDone()) {
				if (!MyPrefs.isTherapyFinished()) {
					if (MyPrefs.isStageOneCompleted()) {
						super.finish();
						startActivity(new Intent(ActivityStartup.this, ActivityStage2.class));
					} else {
						super.finish();
						startActivity(new Intent(ActivityStartup.this, ActivityStage1.class));
					}
				} else {
					super.finish();
					startActivity(new Intent(ActivityStartup.this, ActivityStageFinal.class));
				}
			} else {
				super.finish();
				startActivity(new Intent(ActivityStartup.this, PersonalInfoActivity.class));
			}
		} else {
			getSupportFragmentManager().beginTransaction().add(R.id.container, new Fragment1Language()).commit();
		}
	}
}