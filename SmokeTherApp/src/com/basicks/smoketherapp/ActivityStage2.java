package com.basicks.smoketherapp;

import android.app.Notification;
import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.LocalBroadcastManager;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.basicks.smoketherapp.models.Commons;
import com.basicks.smoketherapp.models.Constants;
import com.basicks.smoketherapp.models.MyPrefs;
import com.basicks.smoketherapp.notification.CigaretteNotification;
import com.basicks.smoketherapp.service.CigaretteService;
import com.basicks.smoketherapp.service.ForegroundService;
import com.basicks.smoketherapp.settings.MyOptionsMenu;
import com.basicks.smoketherapp.settings.ResetTherapyDialog;
import com.basicks.smoketherapp.sqlite.DataHelper;
import com.basicks.smoketherapp.statistics.ShareContent;
import com.basicks.smoketherapp.statistics.Statistics;
import com.basicks.smoketherapp.statistics.StatisticsStage1;
import com.todddavies.components.progressbar.ProgressWheel;

public class ActivityStage2 extends FragmentActivity {

	private DataHelper db;
	private static CountDownTimer timerTakeCigarette;
	private LinearLayout llProgress;
	private TextView tvProgress, tvCigaretteAllowance, tvStage, tvExtraCigaretteInfo;
	private Button buttonStatistics, buttonWithdraw, buttonExtraCigarette;
	private ProgressWheel pw;
	private int cigarettesSmokedToday, allowedCigarette, cigarettesForToday, cigarettesBeingAllowedForToday;
	private int stage, dayOfTherapy;
	private BroadcastReceiver receiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {
			initialize();
		}
	};

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_stage2);
		Commons.showOptionsMenu(this);
		db = new DataHelper(this);
		initViews();
	}

	@Override
	protected void onStart() {
		super.onStart();
		MyPrefs.loadSharedPreferences(this);
		checkForExtraCigarette();
		initialize();
	}

	private void initialize() {
		initVariables();
		if (cigarettesForToday > 0 && allowedCigarette == 0) {
			enableProgressWheel(false);
			showWithdrawButton();
		} else if (cigarettesForToday == 0 && !MyPrefs.isExtraCigaretteDrawn()) {
			enableProgressWheel(false);
			showExtraCigaretteButton();
		} else if (cigarettesForToday < 0 && dayOfTherapy >= db.getTherapyLength()) {
			finishTherapy();
		} else if (MyPrefs.isExtraCigaretteSmoked()) {
			tvExtraCigaretteInfo.setVisibility(View.VISIBLE);
			buttonExtraCigarette.setVisibility(View.GONE);
			buttonWithdraw.setVisibility(View.GONE);
			enableProgressWheel(false);
		} else {
			enableProgressWheel(true);
			buttonWithdraw.setVisibility(View.GONE);
			buttonExtraCigarette.setVisibility(View.GONE);
		}
		updateViews();
	}

	private void checkForExtraCigarette() {
		if (MyPrefs.isExtraCigaretteSmoked()) {
			tvExtraCigaretteInfo.setVisibility(View.VISIBLE);
			buttonExtraCigarette.setVisibility(View.GONE);
			buttonWithdraw.setVisibility(View.GONE);
		} else {
			tvExtraCigaretteInfo.setVisibility(View.GONE);
			pw.setOnTouchListener(new OnTouchListener() {

				@Override
				public boolean onTouch(View v, MotionEvent event) {
					switch (event.getAction()) {
					case MotionEvent.ACTION_DOWN:
						startRegisterCigaretteTimer();
						break;
					case MotionEvent.ACTION_UP:
						cancelTimer();
						break;
					case MotionEvent.ACTION_CANCEL:
						cancelTimer();
						break;
					default:
						break;
					}
					return true;
				}
			});
		}
	}

	private void initVariables() {
		stage = Commons.getTherapyStage(this);
		dayOfTherapy = MyPrefs.getDayOfTherapy();
		cigarettesSmokedToday = MyPrefs.getCigarettesSmokedToday();
		cigarettesBeingAllowedForToday = MyPrefs.getCigarettesBeingAllowedForToday();
		if (MyPrefs.isExtraCigaretteDrawn() && !MyPrefs.isExtraCigaretteSmoked()) {
			cigarettesForToday = 1;
			allowedCigarette = 1;
		} else if (MyPrefs.isExtraCigaretteDrawn() && MyPrefs.isExtraCigaretteSmoked()) {
			tvExtraCigaretteInfo.setVisibility(View.VISIBLE);
			buttonExtraCigarette.setVisibility(View.GONE);
			buttonWithdraw.setVisibility(View.GONE);
			enableProgressWheel(false);
		} else {
			allowedCigarette = MyPrefs.getAllowedCigarette();
			cigarettesForToday = db.getCigarettesForToday(dayOfTherapy);
		}
	}

	private void saveVariables() {
		MyPrefs.setCigarettesSmokedToday(cigarettesSmokedToday, this);
		MyPrefs.setCigarettesBeingAllowedForToday(cigarettesBeingAllowedForToday, this);
		MyPrefs.setAllowedCigarette(allowedCigarette, this);
		db.updateCigaretteForToday(cigarettesForToday, dayOfTherapy);
		db.updateCigarettesSmokedToday(cigarettesSmokedToday, dayOfTherapy);
	}

	@Override
	protected void onResume() {
		super.onResume();
		CigaretteNotification.cancelNotification(this);
		MyPrefs.setActivity2Visible(true, this);
		MyPrefs.setHasActivityBeenStartedToday(true, this);
		LocalBroadcastManager.getInstance(this).registerReceiver(receiver, new IntentFilter(Constants.CIG_INTENT));
		startService(new Intent(this, CigaretteService.class));
	}

	private void startRegisterCigaretteTimer() {
		timerTakeCigarette = new CountDownTimer(2000, 54) {
			public void onFinish() {
				if (MyPrefs.isExtraCigaretteDrawn()) {
					MyPrefs.setExtraCigaretteSmoked(true, ActivityStage2.this);
					cigarettesForToday = 0;
					allowedCigarette = 0;
					db.updateCigaretteForToday(cigarettesForToday, dayOfTherapy);
				} else {
					cigarettesSmokedToday++;
					cigarettesForToday--;
					allowedCigarette--;
					updateNotification();
				}
				saveVariables();
				cancelTimer();
				initialize();
			}

			@Override
			public void onTick(long millisUntilFinished) {
				for (int i = 0; i < 10; i++) {
					pw.incrementProgress();
				}
			}
		}.start();
	}

	private void updateNotification() {
		Notification notification = ForegroundService.buildForeground(this, allowedCigarette);
		NotificationManager mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
		mNotificationManager.notify(Constants.FOREGROUND_ID, notification);
	}

	private void cancelTimer() {
		if (timerTakeCigarette != null) {
			timerTakeCigarette.cancel();
		}
		pw.resetCount();
		pw.clearFocus();
	}

	private void enableProgressWheel(boolean b) {
		pw.resetCount();
		pw.setEnabled(b);
		pw.clearFocus();
	}

	private void updateViews() {
		tvStage.setText(getString(R.string.stage) + " " + stage);
		llProgress.setLayoutParams(Commons.getProgressWidth(this));
		tvProgress.setLayoutParams(Commons.getButtonWidth(this));
		tvProgress.setText(MyPrefs.getProgressPercents() + "%");
		tvCigaretteAllowance.setText(getString(R.string.cig_allowance) + " " + allowedCigarette);
		if (Constants.DEBUG) {
			showDebugToast();
		}
	}

	@Override
	protected void onStop() {
		super.onStop();
		MyPrefs.setActivity2Visible(false, this);
		LocalBroadcastManager.getInstance(this).unregisterReceiver(receiver);
	}

	private void finishTherapy() {
		finish();
		MyPrefs.setIsTherapyFinished(true, this);
		startActivity(new Intent(this, ActivityStageFinal.class));
	}

	private void showWithdrawButton() {
		buttonWithdraw.setVisibility(View.VISIBLE);
		buttonExtraCigarette.setVisibility(View.GONE);
		buttonWithdraw.setOnTouchListener(Commons.getOnTouchListener(this, buttonWithdraw));
		buttonWithdraw.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Toast.makeText(ActivityStage2.this, "Cigarette withdrawn from daily allowance!", Toast.LENGTH_LONG).show();
				cigarettesBeingAllowedForToday++;
				allowedCigarette++;
				enableButtonWithdrawTimer();
				saveVariables();
				initialize();
				updateNotification();
			}
		});
	}

	protected void enableButtonWithdrawTimer() {
		buttonWithdraw.setEnabled(false);
		new CountDownTimer(10000, 1000) {
			public void onFinish() {
				buttonWithdraw.setEnabled(true);
			}

			@Override
			public void onTick(long millisUntilFinished) {
			}
		}.start();
	}

	private void showExtraCigaretteButton() {
		if (MyPrefs.isExtraCigaretteSmoked()) {
			buttonExtraCigarette.setVisibility(View.GONE);
			buttonWithdraw.setVisibility(View.GONE);
		} else {
			buttonExtraCigarette.setVisibility(View.VISIBLE);
			buttonExtraCigarette.setEnabled(true);
			buttonWithdraw.setVisibility(View.GONE);
			buttonExtraCigarette.setOnTouchListener(Commons.getOnTouchListener(this, buttonExtraCigarette));
			buttonExtraCigarette.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					FragmentManager fm = getSupportFragmentManager();
					ExtraCigDialog fragment = new ExtraCigDialog();
					fragment.show(fm, null);
				}
			});
		}
	}

	private void initViews() {
		tvProgress = (TextView) findViewById(R.id.tvProgress);
		llProgress = (LinearLayout) findViewById(R.id.llProgress);
		tvCigaretteAllowance = (TextView) findViewById(R.id.tvCigaretteAllowance);
		buttonStatistics = (Button) findViewById(R.id.buttonStatistics);
		buttonWithdraw = (Button) findViewById(R.id.buttonWithdraw);
		buttonExtraCigarette = (Button) findViewById(R.id.buttonExtraCigarette);
		tvStage = (TextView) findViewById(R.id.tvStage);
		tvExtraCigaretteInfo = (TextView) findViewById(R.id.tvExtraCigaretteInfo);
		pw = (ProgressWheel) findViewById(R.id.pw_spinner);
		if (Commons.getDisplayMetrics(this) < 200) {
			pw.setRimWidth(5);
			pw.setBarWidth(4);
		} else if (Commons.getDisplayMetrics(this) >= 320) {
			pw.setRimWidth(12);
			pw.setBarWidth(9);
		}
		setStatisticsButton();
	}

	private void setStatisticsButton() {
		buttonStatistics.setOnTouchListener(Commons.getOnTouchListener(ActivityStage2.this, buttonStatistics));
		buttonStatistics.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				if (stage == 2) {
					Intent intent = new Intent(ActivityStage2.this, StatisticsStage1.class);
					startActivity(intent);
				} else {
					Intent intent = new Intent(ActivityStage2.this, Statistics.class);
					intent.putExtra("stage", stage);
					startActivity(intent);
				}
			}
		});
	}

	/** Options Menu */
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		if (Constants.DEBUG) {
			getMenuInflater().inflate(R.menu.my_menu_debug_2, menu);
		} else {
			getMenuInflater().inflate(R.menu.my_menu, menu);
		}
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.action_tutorial:
			startActivity(new Intent(this, ActivityTutorial.class));
			return true;
		case R.id.action_about:
			startActivity(new Intent(this, ActivityAbout.class));
			return true;
		case R.id.action_contact:
			MyOptionsMenu.getContactMenu(this);
			return true;
		case R.id.action_share:
			ShareContent.postDownload(this);
			return true;
		case R.id.action_donate:
			startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(Constants.DONATE_URL)));
			return true;
		case R.id.action_reset:
			ResetTherapyDialog dlg = new ResetTherapyDialog(this);
			dlg.showDialog();
			return true;
		case R.id.action_send_db:
			MyOptionsMenu.sendDatabase(this);
			return true;
		case R.id.action_exit:
			finish();
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}

	/* DEBUG */
	private void showDebugToast() {
		int cig_allowed = MyPrefs.getCigarettesBeingAllowedForToday();
		int cig_smoked = MyPrefs.getCigarettesSmokedToday();
		int cig_allowed_now = MyPrefs.getAllowedCigarette();
		int day = MyPrefs.getDayOfTherapy();
		int cig_left_for_today = db.getCigarettesForToday(day);
		String s = "cig_allowed_now: " + cig_allowed_now + "\ncig_smoked_today: " + cig_smoked + "\ncig_been_allowed: " + cig_allowed
				+ "\nday_of_therapy: " + day + "\ncig_left_for_today: " + cig_left_for_today;
		Toast.makeText(this, s, Toast.LENGTH_SHORT).show();
	}
}