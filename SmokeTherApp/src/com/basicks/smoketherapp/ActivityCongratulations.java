package com.basicks.smoketherapp;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

import com.basicks.smoketherapp.models.Commons;

public class ActivityCongratulations extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_congratulations);
		Button buttonBack = (Button) findViewById(R.id.buttonBack);
		buttonBack.setOnTouchListener(Commons.getOnTouchListener(ActivityCongratulations.this, buttonBack));
		buttonBack.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				finish();
			}
		});
	}
}