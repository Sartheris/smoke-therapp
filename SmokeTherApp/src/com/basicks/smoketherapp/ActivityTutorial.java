package com.basicks.smoketherapp;

import com.basicks.smoketherapp.initialsetup.Fragment3Tutorial;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;

public class ActivityTutorial extends FragmentActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_tutorial);
		getSupportFragmentManager().beginTransaction().add(R.id.container, new Fragment3Tutorial()).commit();
	}
}