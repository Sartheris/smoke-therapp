package com.basicks.smoketherapp.statistics;

import com.basicks.smoketherapp.R;
import com.basicks.smoketherapp.models.Commons;
import com.basicks.smoketherapp.sqlite.DataHelper;
import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

public class StatisticsStage4 extends Activity {

	private Button buttonShare;
	private TextView tvDays, tvNicotine;
	private String nicotineLowered;
	private DataHelper db;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_statistics_stage4);
		db = new DataHelper(StatisticsStage4.this);
		tvDays = (TextView) findViewById(R.id.tvDays);
		nicotineLowered = db.getNicotineLoweredForStageFour() + "%";
		tvNicotine = (TextView) findViewById(R.id.tvNicotine);
		tvNicotine.setText(nicotineLowered);
		tvDays.setText(db.getDaysTakenToCompleteStage(4) + "");
		buttonShare = (Button) findViewById(R.id.buttonShare);
		buttonShare.setOnTouchListener(Commons.getOnTouchListener(StatisticsStage4.this, buttonShare));
		buttonShare.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				ShareContent.postStatus(StatisticsStage4.this, nicotineLowered);
			}
		});
	}
}