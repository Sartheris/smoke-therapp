package com.basicks.smoketherapp.statistics;

import com.basicks.smoketherapp.R;
import com.basicks.smoketherapp.models.Commons;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

public class Statistics extends Activity {

	private Button buttonStage1, buttonStage2, buttonStage3, buttonStage4, buttonComplete;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_statistics);
		buttonStage1 = (Button) findViewById(R.id.buttonStage1);
		buttonStage2 = (Button) findViewById(R.id.buttonStage2);
		buttonStage3 = (Button) findViewById(R.id.buttonStage3);
		buttonStage4 = (Button) findViewById(R.id.buttonStage4);
		buttonComplete = (Button) findViewById(R.id.buttonComplete);
		if (getIntent().hasExtra("stage")) {
			int stage = getIntent().getIntExtra("stage", 3);
			if (stage == 3) {
				buttonStage3.setVisibility(View.GONE);
				buttonStage4.setVisibility(View.GONE);
				buttonComplete.setVisibility(View.GONE);
			} else if (stage == 4) {
				buttonStage4.setVisibility(View.GONE);
				buttonComplete.setVisibility(View.GONE);
			}
		}
		buttonStage1.setOnTouchListener(Commons.getOnTouchListener(this, buttonStage1));
		buttonStage2.setOnTouchListener(Commons.getOnTouchListener(this, buttonStage2));
		buttonStage3.setOnTouchListener(Commons.getOnTouchListener(this, buttonStage3));
		buttonStage4.setOnTouchListener(Commons.getOnTouchListener(this, buttonStage4));
		buttonComplete.setOnTouchListener(Commons.getOnTouchListener(this, buttonComplete));
		buttonStage1.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent intent = new Intent(Statistics.this, StatisticsStage1.class);
				startActivity(intent);
			}
		});
		buttonStage2.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent intent = new Intent(Statistics.this, StatisticsStage2.class);
				startActivity(intent);
			}
		});
		buttonStage3.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent intent = new Intent(Statistics.this, StatisticsStage3.class);
				startActivity(intent);
			}
		});
		buttonStage4.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent intent = new Intent(Statistics.this, StatisticsStage4.class);
				startActivity(intent);
			}
		});
		buttonComplete.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent intent = new Intent(Statistics.this, StatisticsComplete.class);
				startActivity(intent);
			}
		});
	}
}