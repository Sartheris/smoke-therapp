package com.basicks.smoketherapp.statistics;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.basicks.smoketherapp.R;
import com.basicks.smoketherapp.models.MyPrefs;
import com.basicks.smoketherapp.sqlite.DataHelper;

public class StatisticsStage1 extends Activity {

	private TextView tvCigarettes;
	private DataHelper db;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_statistics_stage_1);
		db = new DataHelper(StatisticsStage1.this);
		tvCigarettes = (TextView) findViewById(R.id.tvCigarettes);
		tvCigarettes.setText(db.getFirstDayCigarettes() + "");
		LinearLayout ll = (LinearLayout) findViewById(R.id.ll2);
		ll.setVisibility(View.VISIBLE);
		TextView tvDays = (TextView) findViewById(R.id.tvDays);
		if (!MyPrefs.isTherapyFinished()) {
			tvDays.setText(db.getDaysLeftToComplete() + "");
		} else {
			ll.setVisibility(View.GONE);
		}
	}
}