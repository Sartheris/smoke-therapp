package com.basicks.smoketherapp.statistics;

import java.io.File;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;

import com.basicks.smoketherapp.R;
import com.basicks.smoketherapp.models.Constants;

public class ShareContent {

	public static void postStatus(Context context, String nicotineLowered) {
		Intent sharingIntent = new Intent(Intent.ACTION_SEND);
		sharingIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);
		sharingIntent.setType("text/plain");
		String shareBody = context.getString(R.string.less_nicotine_share_1) + " [" + nicotineLowered + "] "
				+ context.getString(R.string.less_nicotine_share_2);
		sharingIntent.putExtra(Intent.EXTRA_SUBJECT, Constants.APP_NAME);
		sharingIntent.putExtra(Intent.EXTRA_TEXT, shareBody);
		context.startActivity(Intent.createChooser(sharingIntent, "Share via"));
	}

	public static void postGraph(Context context, String dir) {
		Intent sharingIntent = new Intent(Intent.ACTION_SEND);
		sharingIntent.setType("image/*");
		sharingIntent.putExtra(Intent.EXTRA_TEXT, Constants.APP_NAME);
		sharingIntent.putExtra(Intent.EXTRA_SUBJECT, Constants.APP_NAME);
		sharingIntent.putExtra(Intent.EXTRA_STREAM, Uri.fromFile(new File(dir)));
		context.startActivity(Intent.createChooser(sharingIntent, "Share image"));
	}
	
	public static void postDownload(Context context) {
		Intent sharingIntent = new Intent(Intent.ACTION_SEND);
		sharingIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);
		sharingIntent.setType("text/plain");
		sharingIntent.putExtra(Intent.EXTRA_SUBJECT, Constants.APP_NAME);
		sharingIntent.putExtra(Intent.EXTRA_TEXT, Constants.SHARE_TEXT + Constants.DOWNLOAD_LINK);
		context.startActivity(Intent.createChooser(sharingIntent, "Share via"));
	}
}