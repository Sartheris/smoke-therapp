package com.basicks.smoketherapp.statistics;

import com.basicks.smoketherapp.R;
import com.basicks.smoketherapp.models.Commons;
import com.basicks.smoketherapp.sqlite.DataHelper;
import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

public class StatisticsStage3 extends Activity {

	private Button buttonShare;
	private String nicotineLowered;
	private TextView tvDays, tvNicotine;
	private DataHelper db;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_statistics_stage3);
		db = new DataHelper(StatisticsStage3.this);
		tvDays = (TextView) findViewById(R.id.tvDays);
		tvNicotine = (TextView) findViewById(R.id.tvNicotine);
		tvDays.setText(db.getDaysTakenToCompleteStage(3) + "");
		nicotineLowered = db.getNicotineLoweredForStageThree() + "%";
		tvNicotine.setText(nicotineLowered);
		buttonShare = (Button) findViewById(R.id.buttonShare);
		buttonShare.setOnTouchListener(Commons.getOnTouchListener(StatisticsStage3.this, buttonShare));
		buttonShare.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				ShareContent.postStatus(StatisticsStage3.this, nicotineLowered);
			}
		});
	}
}