package com.basicks.smoketherapp.statistics;

import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.Canvas;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Environment;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;
import com.basicks.smoketherapp.R;
import com.basicks.smoketherapp.models.Commons;
import com.basicks.smoketherapp.sqlite.DataHelper;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;

public class StatisticsComplete extends Activity {

	private DataHelper db;
	private TextView tvProgress;
	private LineChart chart;
	private Button buttonShare;
	private String nicotineTwo, nicotineThree, nicotineFour;
	private static final String SCREENSHOT_NAME = "smoke_graph.jpg";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_statistics_complete);
		db = new DataHelper(this);
		nicotineTwo = db.getNicotineLoweredForStageTwo();
		nicotineThree = db.getNicotineLoweredForStageThree();
		nicotineFour = db.getNicotineLoweredForStageFour();
		tvProgress = (TextView) findViewById(R.id.tvProgress);
		buttonShare = (Button) findViewById(R.id.buttonShare);
		chart = (LineChart) findViewById(R.id.chart);
		chart.setDescription("Nicotine Lowered");
		buttonShare.setOnTouchListener(Commons.getOnTouchListener(this, buttonShare));
		buttonShare.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				String dir = saveBitmap(chart);
				ShareContent.postGraph(StatisticsComplete.this, dir);
			}
		});
		tvProgress.setText(nicotineFour + "% " + getString(R.string.less_nicotine));
		createGraph();
	}

	private String saveBitmap(View v) {
		Bitmap bitmap = Bitmap.createBitmap(v.getWidth(), v.getHeight(), Config.ARGB_8888);
		Canvas canvas = new Canvas(bitmap);
		v.draw(canvas);
		String root = Environment.getExternalStorageDirectory().toString();
		File myDir = new File(root + "/saved_images");
		myDir.mkdirs();
		File file = new File(myDir, SCREENSHOT_NAME);
		String fullPath = myDir + "/" + SCREENSHOT_NAME;
		if (file.exists())
			file.delete();
		try {
			FileOutputStream out = new FileOutputStream(file);
			bitmap.compress(Bitmap.CompressFormat.JPEG, 90, out);
			out.flush();
			out.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return fullPath;
	}

	public void createGraph() {
		chart.setTouchEnabled(false);
		chart.setDragEnabled(false);
		chart.setHighlightEnabled(false);
		chart.setDrawYLabels(true);
		chart.setDrawXLabels(true);
		chart.animateXY(2000, 2000);
		chart.setDrawBorder(true);
		chart.setPadding(30, 30, 30, 30);
		setData();
	}

	private void setData() {
		String[] stages = { "Stage1", "Stage2", "Stage3", "Stage4" };
		ArrayList<Entry> yVals = new ArrayList<Entry>();
		yVals.add(new Entry(0.0f, 0));
		yVals.add(new Entry(Float.parseFloat(nicotineTwo), 1));
		yVals.add(new Entry(Float.parseFloat(nicotineThree), 2));
		yVals.add(new Entry(Float.parseFloat(nicotineFour), 3));
		LineDataSet set1 = new LineDataSet(yVals, "");
		set1.setColor(Color.BLACK);
		set1.setCircleColor(Color.BLACK);
		set1.setLineWidth(1f);
		ArrayList<LineDataSet> dataSets = new ArrayList<LineDataSet>();
		dataSets.add(set1);
		LineData data = new LineData(stages, dataSets);
		chart.setData(data);
	}
}